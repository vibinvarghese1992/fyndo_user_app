package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vibinvarghese on 20/10/16.
 */

public class LoginFragment extends Fragment {

    CallbackManager callbackManager;

    SharedPreferences prefs;

    TextView logo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login_fragment, container, false);

        logo = (TextView) rootView.findViewById(R.id.logo_text);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/museo.ttf");
        logo.setTypeface(face);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        /*final LoginButton loginButton = (LoginButton) rootView.findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");*/
        //loginButton.setReadPermissions("user_birthday");
        // If using in a fragment
        //loginButton.setFragment(this);
        // Other app specific specialization

        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress);

        /*loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                loginButton.setVisibility(View.GONE);
            }
        });*/

        /*final ImageView fbLoginButton = (ImageView) rootView.findViewById(R.id.fb_login_button);

        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtain the shared Tracker instance.
                AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("FBLoginClicked");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                loginButton.callOnClick();
                fbLoginButton.setVisibility(View.GONE);

            }
        });*/

        /*TextView skip = (TextView) rootView.findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceId = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                //String timeStamp = String.valueOf(System.currentTimeMillis());
                prefs.edit().putString(Constants.FB_USER_NAME, "skipped_login").apply();
                prefs.edit().putString(Constants.FB_USER_ID, "skipped_login_" + deviceId).apply();
                prefs.edit().putString(Constants.FB_GENDER, "").apply();
                createNewUser();
            }
        });*/

        final Button getStarted = (Button) rootView.findViewById(R.id.get_started);

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getStarted.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                String deviceId = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                //String timeStamp = String.valueOf(System.currentTimeMillis());
                prefs.edit().putString(Constants.FB_USER_NAME, "skipped_login").apply();
                prefs.edit().putString(Constants.FB_USER_ID, "skipped_login_" + deviceId).apply();
                prefs.edit().putString(Constants.FB_GENDER, "").apply();
                checkIfUserExists();
            }
        });

        getStarted.callOnClick();

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        /*loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                prefs.edit().putString(Constants.FB_USER_ID, loginResult.getAccessToken().getUserId()).apply();

                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                String name = "", email = "", gender = "";
                                // Application code
                                try {
                                    email = object.getString("email");
                                    name = object.getString("name");
                                    gender = object.getString("gender");
                                    if (gender.equals("male"))
                                        gender = "M";
                                    else
                                        gender = "F";
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                prefs.edit().putString(Constants.FB_USER_NAME, name).apply();
                                prefs.edit().putString(Constants.FB_USER_EMAIL, email).apply();
                                prefs.edit().putString(Constants.FB_GENDER, gender).apply();

                                checkIfUserExists();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Util.showSnackBar("Something went wrong, please try again", logo);
            }
        });*/

        return rootView;
    }

    protected void postLogin() {

        ((MainActivity) getActivity()).prefs.edit().putBoolean(Constants.IS_LOGGED_IN, true).apply();
        ((MainActivity) getActivity()).postLogin();
        //getActivity().getFragmentManager().popBackStack();
    }

    protected void checkIfUserExists() {
        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "CheckIfUserExists",
                Constants.SELECT_USER_BY_FB + prefs.getString(Constants.FB_USER_ID, ""), null, "GET");

        performAsyncTask.execute();
    }

    protected void createNewUser() {
        JSONObject postDataParams = new JSONObject();
        try {
            postDataParams.put("userName", prefs.getString(Constants.FB_USER_NAME, ""));
            postDataParams.put("emailId", prefs.getString(Constants.FB_USER_EMAIL, ""));
            postDataParams.put("fbProfile", prefs.getString(Constants.FB_USER_ID, ""));
            postDataParams.put("gender", prefs.getString(Constants.FB_GENDER, "M"));
            postDataParams.put("dateOfBirth", String.valueOf(System.currentTimeMillis()));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "CreateNewUser",
                Constants.INSERT_USER, postDataParams, "POST");

        performAsyncTask.execute();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }
}
