package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 27/09/16.
 */

public class EateryFragment extends Fragment {

    ArrayList<String> shopCategoryEatery;
    ListView listView;
    CategoryListAdapter categoryListAdapter;

    // Arraylist for checked item in the lesson view
    ArrayList<String> checkedlessons = new ArrayList<String>();

    @SuppressLint("ValidFragment")
    public EateryFragment(ArrayList<String> shopCategoryEatery) {
        this.shopCategoryEatery = shopCategoryEatery;
    }

    public EateryFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.eatery_fragment, container, false);

        listView = (ListView) rootView.findViewById(R.id.eatery_list);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Gson gson = new Gson();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String json = prefs.getString(Constants.SELECTED_EATERY_CATEGORIES, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        ArrayList<String> selectedItems = gson.fromJson(json, type);

        categoryListAdapter = new CategoryListAdapter(getActivity(), shopCategoryEatery, selectedItems);
        listView.setAdapter(categoryListAdapter);
    }
}
