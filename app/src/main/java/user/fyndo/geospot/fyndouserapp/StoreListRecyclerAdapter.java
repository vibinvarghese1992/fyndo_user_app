package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.Text;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vibinvarghese on 03/09/16.
 */

public class StoreListRecyclerAdapter extends RecyclerView.Adapter<StoreListRecyclerAdapter.ViewHolder> {

    ArrayList<StoreItemModel> storeItemModelArrayList;
    Context context;
    LayoutInflater inflater;
    List<String> progressBarColors;
    List<String> imageFrameColors;

    public StoreListRecyclerAdapter(Context context, ArrayList<StoreItemModel> storeItemModelArrayList) {
        this.storeItemModelArrayList = storeItemModelArrayList;
        this.context = context;
        this.progressBarColors = Arrays.asList(context.getApplicationContext().getResources().getStringArray(R.array.progress_color));
        this.imageFrameColors = Arrays.asList(context.getApplicationContext().getResources().getStringArray(R.array.frame_colors));

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView shopImage, callIcon, mapIcon;
        TextView shopName, shopArea, shopPhone, shopCategories, shopDistance;
        ProgressBar progressBar;
        LinearLayout followLayout;
        FrameLayout imageFrame;

        public ViewHolder(View view) {
            super(view);
            mapIcon = (ImageView) view.findViewById(R.id.maps);
            shopImage = (ImageView) view.findViewById(R.id.shop_image);
            callIcon = (ImageView) view.findViewById(R.id.call);
            shopName = (TextView) view.findViewById(R.id.shop_name);
            shopArea = (TextView) view.findViewById(R.id.shop_area);
            shopPhone = (TextView) view.findViewById(R.id.shop_phone);
            shopCategories = (TextView) view.findViewById(R.id.sub_categories);
            shopDistance = (TextView) view.findViewById(R.id.shop_distance);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            followLayout = (LinearLayout) view.findViewById(R.id.follow_layout);
            imageFrame = (FrameLayout) view.findViewById(R.id.image_frame);

        }
    }


    @Override
    public StoreListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.store_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final StoreListRecyclerAdapter.ViewHolder holder, final int position) {

        final StoreItemModel storeItemModel = storeItemModelArrayList.get(position);

        holder.shopName.setText(storeItemModel.getShopName());
        holder.shopArea.setText(storeItemModel.getShopArea());
        holder.shopPhone.setText(storeItemModel.getShopPhone());


        double distance = Util.distance(context, storeItemModel.getLat(), storeItemModel.getLng());

        distance = distance / 1000;

        if (distance > 100) {
            holder.shopDistance.setText(" ~5" + " km away");
        } else {
            holder.shopDistance.setText(String.format("%.2f", distance) + " km away");
        }

        String catList = "";
        try {
            JSONArray cats = new JSONArray(storeItemModel.getBizCategories());
            for (int k = 0; k < cats.length(); k++) {
                JSONObject object = cats.getJSONObject(k);
                String subList = "";
                if (object.has("Men")) {
                    subList = object.getString("Men");
                    subList = subList.replace("[", "");
                    subList = subList.replace("]", "");
                    subList = subList.replace("\"", "");
                    subList = subList.replace("\\u0026", "&");
                }
                if (object.has("Women")) {
                    subList = object.getString("Women");
                    subList = subList.replace("[", "");
                    subList = subList.replace("]", "");
                    subList = subList.replace("\"", "");
                    subList = subList.replace("\\u0026", "&");
                }
                catList += subList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + storeItemModel.getShopPhone()));
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions((MainActivity) context, new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.CALL_PHONE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);

                    Util.showSnackBar("Please grant permission to make a call", holder.callIcon);
                    return;
            }
                AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("CallTriggered");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                context.startActivity(intent);
        }});

        holder.mapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).launchBizDetailFragment(storeItemModel.getShopName(), storeItemModel.getShopArea(), storeItemModel.getBizId(), storeItemModel.getLat(), storeItemModel.getLng());
            }
        });

        holder.shopCategories.setText(catList);

        if (((MainActivity) context).userFollowedBizIds.contains(storeItemModel.getBizId())) {
            ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_filled));
            ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_hollow));
            ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(android.R.color.darker_gray));
        }

        holder.followLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (storeItemModel.getBizId() == 0) {
                    Util.showSnackBar("Shop Info not available, please try again later", holder.followLayout);
                    return;
                }

                JSONObject postDataParams = new JSONObject();
                try {
                    postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    postDataParams.put("bizId", storeItemModel.getBizId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (((MainActivity) context).userFollowedBizIds.contains(storeItemModel.getBizId())) {
                    ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_hollow));
                    ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(android.R.color.darker_gray));

                    ((MainActivity) context).userFollowedBizIds.remove(new Integer(storeItemModel.bizId));


                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "UnFollowBiz",
                            Constants.DELETE_FAVOURITE_BIZ_INFO, postDataParams, "POST");

                    performAsyncTask.execute();

                    return;
                }


                ((MainActivity) context).userFollowedBizIds.add(storeItemModel.bizId);

                ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_filled));
                ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(R.color.colorPrimary));

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "FollowBiz",
                        Constants.INSERT_FAVOURITE_BIZ_INFO, postDataParams, "POST");

                performAsyncTask.execute();

            }
        });

        int colorIndex = position % 10;

        holder.imageFrame.setBackgroundColor(Color.parseColor(imageFrameColors.get(colorIndex)));
        holder.progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor(progressBarColors.get(colorIndex)), PorterDuff.Mode.SRC_IN);

        holder.progressBar.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load(Constants.ROOT_URL + storeItemModel.getShopImage())
                .resize(300, 300)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .error(R.drawable.image_place_holder)
                .into(holder.shopImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.imageFrame.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    }

                    @Override
                    public void onError() {

                    }
                });

    }

    @Override
    public int getItemCount() {
        return storeItemModelArrayList.size();
    }

}

