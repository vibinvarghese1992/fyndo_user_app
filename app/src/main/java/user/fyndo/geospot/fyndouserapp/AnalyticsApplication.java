package user.fyndo.geospot.fyndouserapp;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by vibinvarghese on 03/12/16.
 */

public class AnalyticsApplication extends Application {
    private Tracker mTracker;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Report FirebaseCrash Exception if application crashed*/
        /*Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {

                logLargeString(e.toString());
                *//** Check whether it is development or release mode*//**//**//**//*
                *//**//*if(BuildConfig.REPORT_CRASH)
                {
                    FirebaseCrash.report( e);
                }*//*
            }
        });*/

    }

    public void logLargeString(String str) {
        if(str.length() > 3000) {
            Log.i("Error", str.substring(0, 3000));
            logLargeString(str.substring(3000));
        } else {
            Log.i("Error", str); // continuation
        }
    }
}