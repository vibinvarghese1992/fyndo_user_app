package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 21/01/17.
 */

public class SubCategorySelectionDialog extends DialogFragment {

    Button go;

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        /*if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sub_category_selection_dialog, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        final Drawable d = new ColorDrawable(Color.TRANSPARENT);
        getDialog().getWindow().setBackgroundDrawable(d);

        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.men_main), R.id.men, ((MainActivity) getActivity()).men, ((MainActivity) getActivity()).selectedMen);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.women_main), R.id.women, ((MainActivity) getActivity()).women, ((MainActivity) getActivity()).selectedWomen);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.kids_main), R.id.kids, ((MainActivity) getActivity()).babyNKids, ((MainActivity) getActivity()).selectedBabyNKids);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.electronics_main), R.id.electronics, ((MainActivity) getActivity()).consumerElectronics, ((MainActivity) getActivity()).selectedConsumerElectronics);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.appliance_main), R.id.appliance, ((MainActivity) getActivity()).homeAppliances, ((MainActivity) getActivity()).selectedHomeAppliances);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.furniture_main), R.id.furniture, ((MainActivity) getActivity()).homeFurnitures, ((MainActivity) getActivity()).selectedHomeFurnitures);

        go = (Button) rootView.findViewById(R.id.discovering);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFilter();
            }
        });


        CheckBox selectAllMen = (CheckBox) rootView.findViewById(R.id.select_all_men);
        CheckBox selectAllWomen = (CheckBox) rootView.findViewById(R.id.select_all_women);
        CheckBox selectAllKids = (CheckBox) rootView.findViewById(R.id.select_all_kids);
        CheckBox selectAllElectronics = (CheckBox) rootView.findViewById(R.id.select_all_electronics);
        CheckBox selectAllAppliances = (CheckBox) rootView.findViewById(R.id.select_all_appliances);
        CheckBox selectAllFurniture = (CheckBox) rootView.findViewById(R.id.select_all_furniture);

        selectAllMen.setOnCheckedChangeListener(checkedChangeListener);
        selectAllWomen.setOnCheckedChangeListener(checkedChangeListener);
        selectAllKids.setOnCheckedChangeListener(checkedChangeListener);
        selectAllElectronics.setOnCheckedChangeListener(checkedChangeListener);
        selectAllAppliances.setOnCheckedChangeListener(checkedChangeListener);
        selectAllFurniture.setOnCheckedChangeListener(checkedChangeListener);

        return rootView;
    }

    protected void applyFilter() {

        if ((((MainActivity) getActivity()).selectedMen.size() + ((MainActivity) getActivity()).selectedWomen.size()) < 1) {
            Util.showSnackBar("Please select at least 1 categories", go);
            return;
        }



        ((MainActivity) getActivity()).currentFeedCount = 20;
        ((MainActivity) getActivity()).dynamicFeed = 0;
        ((MainActivity) getActivity()).feedArrayList.clear();
        ((MainActivity) getActivity()).feedListRecyclerAdapter.notifyDataSetChanged();


        JSONArray sample = new JSONArray();

        if (((MainActivity) getActivity()).selectedMen.size() > 0)
            sample.put("Men");
        if (((MainActivity) getActivity()).selectedWomen.size() > 0)
            sample.put("Women");
        if (((MainActivity) getActivity()).selectedConsumerElectronics.size() > 0)
            sample.put("Consumer Electronics");
        if (((MainActivity) getActivity()).selectedHomeAppliances.size() > 0)
            sample.put("Home Appliances");
        if (((MainActivity) getActivity()).selectedHomeFurnitures.size() > 0)
            sample.put("Home Furnitures");
        if (((MainActivity) getActivity()).selectedBabyNKids.size() > 0)
            sample.put("Kids");

        ((MainActivity) getActivity()).currentSelected = sample;

        ((MainActivity) getActivity()).makeCustomRequestToPopulateAllFeeds(true, false);

        ((MainActivity) getActivity()).setSelectedCategoriesText();

        dismiss();
    }

    protected void insertPreferences(FlexboxLayout parentView, int id, ArrayList<String> preferences, ArrayList<String> selectedItems) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < preferences.size(); i++) {

            String category = preferences.get(i);
            View view = (View) inflater.inflate(R.layout.single_item_textview, null);

            if (selectedItems.contains(category)) {
                ((TextView) view.findViewById(R.id.item1)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
                ((TextView) view.findViewById(R.id.item1)).setTextColor(getActivity().getResources().getColor(android.R.color.black));
            }
            TextView itemTextView1 = (TextView) view.findViewById(R.id.item1);
            itemTextView1.setOnClickListener(onClickListener);
            itemTextView1.setText(category);
            parentView.addView(view);
        }
    }

    protected ArrayList<String> processSelectedItem(ArrayList<String> selectedPrefs, String choice, View v) {
        if (selectedPrefs.indexOf(choice) > -1) {
            selectedPrefs.remove(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.unselected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        } else {
            selectedPrefs.add(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.black));
        }
        return selectedPrefs;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int selctedId = ((FlexboxLayout) v.getParent().getParent()).getId();

            switch (selctedId) {
                case R.id.men_main:
                    ((MainActivity) getActivity()).selectedMen = processSelectedItem(((MainActivity) getActivity()).selectedMen, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.women_main:
                    ((MainActivity) getActivity()).selectedWomen = processSelectedItem(((MainActivity) getActivity()).selectedWomen, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.kids_main:
                    ((MainActivity) getActivity()).selectedBabyNKids = processSelectedItem(((MainActivity) getActivity()).selectedBabyNKids, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.electronics_main:
                    ((MainActivity) getActivity()).selectedConsumerElectronics = processSelectedItem(((MainActivity) getActivity()).selectedConsumerElectronics, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.appliance_main:
                    ((MainActivity) getActivity()).selectedHomeAppliances = processSelectedItem(((MainActivity) getActivity()).selectedHomeAppliances, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.furniture_main:
                    ((MainActivity) getActivity()).selectedHomeFurnitures = processSelectedItem(((MainActivity) getActivity()).selectedHomeFurnitures, ((TextView) v).getText().toString(), v);
                    break;
            }
        }
    };

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            processSelectAll(buttonView, isChecked);
        }
    };

    @SuppressLint("WrongViewCast")
    protected void processSelectAll(CompoundButton button, boolean isChecked) {
        FlexboxLayout parentView;
        switch (button.getId()) {
            case R.id.select_all_men:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.men_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedMen, isChecked, parentView);
                break;
            case R.id.select_all_women:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.women_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedWomen, isChecked, parentView);
                break;
            case R.id.select_all_kids:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.kids_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedBabyNKids, isChecked, parentView);
                break;
            case R.id.select_all_electronics:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.electronics_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedConsumerElectronics, isChecked, parentView);
                break;
            case R.id.select_all_appliances:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.appliance_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedHomeAppliances, isChecked, parentView);
                break;
            case R.id.select_all_furniture:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.furniture_main);
                processSelectAllElements(((MainActivity) getActivity()).selectedHomeFurnitures, isChecked, parentView);
                break;
        }
    }

    protected void processSelectAllElements(ArrayList<String> checkList, boolean isChecked, FlexboxLayout parentView) {
        if (isChecked) {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (!checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        } else {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        }
    }
}
