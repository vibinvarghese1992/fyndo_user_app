package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.ExpandedMenuView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vibinvarghese on 03/09/16.
 */

public class FeedListRecyclerAdapter extends RecyclerView.Adapter<FeedListRecyclerAdapter.ViewHolder> {

    ArrayList<FeedItemModel> feedItemModelArrayList;
    Context context;
    List<String> progressBarColors;
    List<String> imageFrameColors;
    String likedFeeds;
    protected int lastPosition = -1;
    boolean continueRequests = true;
    LayoutInflater inflater;
    boolean isFavouriteFragment;
    Typeface bold, regular;

    public FeedListRecyclerAdapter(Context context, ArrayList<FeedItemModel> feedItemModelArrayList, String likedFeeds, @Nullable boolean isFavouriteFragment) {
        this.feedItemModelArrayList = feedItemModelArrayList;
        this.context = context;
        this.progressBarColors = Arrays.asList(context.getApplicationContext().getResources().getStringArray(R.array.progress_color));
        this.imageFrameColors = Arrays.asList(context.getApplicationContext().getResources().getStringArray(R.array.frame_colors));
        this.likedFeeds = likedFeeds;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.isFavouriteFragment = isFavouriteFragment;
        this.bold = Typeface.createFromAsset(context.getAssets(), "fonts/bold.ttf");
        this.regular = Typeface.createFromAsset(context.getAssets(), "fonts/regular.ttf");
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, shopArea, shopName, likes, distance, progressText, time, deal;
        public ImageView shopImage, heart, call, map, favImage, shareLayout;
        CustomViewPager viewPager;
        LinearLayout pager_indicator, shopDetailLayout, followLayout;
        ViewPagerAdapterForFeeds mAdapter;
        private int dotsCount;
        private ImageView[] dots;
        ProgressBar progressBar, lastItemProgressBar, distanceProgress;
        FrameLayout imageFrame;
        LinearLayout cardView, likeLayout, tagsLinearLayout, tagsLinearLayout2;
        CardView mainCard;
        Spinner ratingSpinner;

        HorizontalScrollView horizonatal_scrollview2;

        public ViewHolder(View view) {
            super(view);
            ratingSpinner = (Spinner) view.findViewById(R.id.rate_spinner);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            deal = (TextView) view.findViewById(R.id.deal_tag);
            time = (TextView) view.findViewById(R.id.time);
            progressText = (TextView) view.findViewById(R.id.progress_text);
            distance = (TextView) view.findViewById(R.id.distance);
            likes = (TextView) view.findViewById(R.id.likes);
            shopArea = (TextView) view.findViewById(R.id.shop_area);
            shopName = (TextView) view.findViewById(R.id.shop_name);
            //description = (TextView) view.findViewById(R.id.description);
            shopImage = (ImageView) view.findViewById(R.id.shop_image);
            followLayout = (LinearLayout) view.findViewById(R.id.follow_layout);
            heart = (ImageView) view.findViewById(R.id.heart_image);
            call = (ImageView) view.findViewById(R.id.call);
            map = (ImageView) view.findViewById(R.id.maps);
            favImage = (ImageView) view.findViewById(R.id.fav_image);
            viewPager = (CustomViewPager) view.findViewById(R.id.pager_introduction);
            pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            distanceProgress = (ProgressBar) view.findViewById(R.id.distance_progress);
            lastItemProgressBar = (ProgressBar) view.findViewById(R.id.last_item_progress);
            imageFrame = (FrameLayout) view.findViewById(R.id.image_frame);
            cardView = (LinearLayout) view.findViewById(R.id.card_item);
            likeLayout = (LinearLayout) view.findViewById(R.id.like_layout);
            tagsLinearLayout = (LinearLayout) view.findViewById(R.id.row);
            tagsLinearLayout2 = (LinearLayout) view.findViewById(R.id.row2);
            shopDetailLayout = (LinearLayout) view.findViewById(R.id.shop_detail_layout);
            shareLayout = (ImageView) view.findViewById(R.id.new_share);
            mainCard = (CardView) view.findViewById(R.id.main_card);
        }
    }

    protected void createShareFeedUrl(int feedId) {
        JSONObject postDataParams = new JSONObject();

        try {
            postDataParams.put("longDynamicLink", Constants.DEEPLINK_PREFIX + feedId + Constants.DEEPLINK_SUFFIX);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "ShareFeed",
                "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + Constants.FIREBASE_API_KEY, postDataParams, "POST");

        performAsyncTask.execute();

    }

    @Override
    public FeedListRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_item, parent, false);

        return new ViewHolder(itemView);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            /*Animation animation = AnimationUtils.loadAnimation(context, R.anim.right_to_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;*/
        }
    }

    public int getShopImage(String mainCategory) {
        switch (mainCategory) {
            case "Men":
                return R.drawable.men;
            case "Women":
                return R.drawable.women;
            case "Consumer Electronics":
                return R.drawable.consumer_electronics;
            case "Home Appliances":
                return R.drawable.home_appliances;
            case "Home & Furnitures":
                return R.drawable.home_furniture;
            case "Jewellery":
                return R.drawable.jewellery;
            case "Books & More":
                return R.drawable.books_more;
            case "Baby & Kids":
                return R.drawable.baby_kids;
            case "Sports & Fitness":
                return R.drawable.sports_fitness;
            case "Automobile & Accessories":
                return R.drawable.automobile;
            default:
                return R.drawable.store_place_holder;

        }
    }

    protected void launchSubCategoryFeedsFragment(String subCategory, String mainCategory) {
        ((MainActivity) context).launchSubCategoryFeedsFragment(subCategory, mainCategory);
    }

    @Override
    public void onBindViewHolder(final FeedListRecyclerAdapter.ViewHolder holder, final int position) {

        final FeedItemModel feedItemModel = feedItemModelArrayList.get(position);

        if (feedItemModel.getTitle().equals("Last Item")) {
            holder.lastItemProgressBar.setVisibility(View.VISIBLE);
            holder.cardView.setVisibility(View.GONE);

            if (continueRequests) {

                if (((MainActivity) context).subCategoryFeedsFragment != null) {
                    ((MainActivity) context).subCategoryFeedsFragment.makeRequest();
                    ((MainActivity) context).dynamicFeedForSubCategory = feedItemModelArrayList.get(position - 1).getId();
                } else {
                    ((MainActivity) context).dynamicFeed = feedItemModelArrayList.get(position - 1).getId();
                    ((MainActivity) context).makeCustomRequestToPopulateAllFeeds(false, ((MainActivity) context).previousRequestType);
                }
            }

            return;
        } else {
            holder.lastItemProgressBar.setVisibility(View.GONE);
            holder.cardView.setVisibility(View.VISIBLE);
        }

        if (feedItemModel.getDealType().equals("Deals")) {
            holder.deal.setVisibility(View.VISIBLE);
        } else {
            holder.deal.setVisibility(View.GONE);
        }

        if (isFavouriteFragment) {
            holder.favImage.setImageDrawable(context.getResources().getDrawable(R.drawable.remove_favourite));
        }

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData("0", R.drawable.filled_star_rating));
        list.add(new ItemData("1", R.drawable.filled_star_rating));
        list.add(new ItemData("2", R.drawable.filled_star_rating));
        list.add(new ItemData("3", R.drawable.filled_star_rating));
        list.add(new ItemData("4", R.drawable.filled_star_rating));
        list.add(new ItemData("5", R.drawable.filled_star_rating));

        SpinnerAdapter adapter = new SpinnerAdapter((MainActivity) context, R.layout.spinner_layout, R.id.txt, list);
        holder.ratingSpinner.setAdapter(adapter);

        int ratingValue = feedItemModel.getRating();
        holder.ratingSpinner.setSelection(ratingValue);

        holder.ratingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    return;

                JSONObject postDataParams = new JSONObject();
                try {
                    postDataParams.put("feedId", feedItemModel.getId());
                    postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    postDataParams.put("userRating", position);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "InsertFeedRating",
                        Constants.RATE_FEED, postDataParams, "POST");

                performAsyncTask.execute();

                feedItemModelArrayList.get(holder.getPosition()).setRating(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        if (!feedItemModel.getShopImage().equals("")) {
            Picasso.with(context).load(Constants.ROOT_URL + feedItemModel.getShopImage())
                    .resize(56, 56)
                    .centerCrop()
                    .into(holder.shopImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) holder.shopImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                            holder.shopImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError() {
                            holder.shopImage.setImageResource(R.drawable.store);
                        }
                    });
        } else {
            holder.shopImage.setImageResource(R.drawable.store);
        }

        holder.time.setText((String) DateUtils.getRelativeTimeSpanString(feedItemModel.getDateOfPost(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        if (!feedItemModel.getTitle().contains("[")) {
            holder.title.setText(WordUtils.capitalize(feedItemModel.getMainTitle()));
            holder.description.setText(WordUtils.capitalize(feedItemModel.getMainDescription().toString()));
            holder.title.setVisibility(View.VISIBLE);
            holder.description.setVisibility(View.VISIBLE);
        } else {
            holder.title.setVisibility(View.GONE);
            holder.description.setVisibility(View.GONE);
        }
        holder.shopName.setText("@" + WordUtils.capitalize(feedItemModel.getShopName()));
        String[] areaArray = feedItemModel.getShopArea().split(", ");
        String extractedShopArea = feedItemModel.getShopArea();
        for (int k = 0; k < areaArray.length; k++) {
            if (areaArray[k].equals("Chennai")) {
                extractedShopArea = areaArray[k - 1];
                break;
            }
        }
        holder.shopArea.setText(extractedShopArea);


        holder.title.setTypeface(bold);
        holder.description.setTypeface(regular);
        holder.time.setTypeface(bold);
        holder.shopName.setTypeface(regular);
        holder.shopArea.setTypeface(regular);
        holder.distance.setTypeface(regular);

        holder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) context).shareIntentShopArea = holder.shopArea.getText().toString();
                ((MainActivity) context).shareIntentShopName = holder.shopName.getText().toString();
                ((MainActivity) context).sharedFeedItem = feedItemModel;
                AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("ShareIntent");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                createShareFeedUrl(feedItemModel.getId());
            }
        });

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+917349712843"));
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions((MainActivity) context, new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.CALL_PHONE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);

                    Util.showSnackBar("Please grant permission to make a call", ((MainActivity) context).recyclerView);
                    return;
                }

                AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("CallTriggered");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                context.startActivity(intent);
            }
        });

        holder.tagsLinearLayout.removeAllViews();
        holder.tagsLinearLayout2.removeAllViews();


        holder.favImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFavouriteFragment) {
                    JSONObject postDataParams = new JSONObject();
                    try {
                        postDataParams.put("feedId", feedItemModel.getId());
                        postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "InsertFavourite",
                            Constants.INSERT_FAVOURITE, postDataParams, "POST");

                    ((MainActivity) context).userFavouritedFeeds.add(feedItemModel.getId());

                    AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
                    Tracker mTracker = application.getDefaultTracker();
                    mTracker.setScreenName("AddedToFavourite");
                    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                    performAsyncTask.execute();
                } else {
                    JSONObject postDataParams = new JSONObject();
                    try {
                        postDataParams.put("feedId", feedItemModel.getId());
                        postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "DeleteFavourite",
                            Constants.DELETE_FAVOURITE, postDataParams, "POST");

                    performAsyncTask.execute();
                }

            }
        });


        JSONArray titleArray = feedItemModel.getSubCategories();


        for (int i = -1; i < titleArray.length(); i++) {

            View view = (View) inflater.inflate(R.layout.single_item_textview_camera, null);
            View view2 = (View) inflater.inflate(R.layout.single_item_textview_main_unselected, null);

            TextView itemTextView1 = (TextView) view.findViewById(R.id.item1);
            TextView itemTextView2 = (TextView) view2.findViewById(R.id.item1);
            if (i == -1) {
                itemTextView1.setText(feedItemModel.getDescription());
            } else {
                try {
                    final String subcat = titleArray.getString(i);
                    itemTextView1.setText(subcat);
                    itemTextView2.setText(subcat);
                    itemTextView1.setTypeface(bold);
                    itemTextView2.setTypeface(bold);
                    holder.tagsLinearLayout2.addView(view2);
                    itemTextView2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (((MainActivity) context).subCategoryFeedsFragment == null)
                                launchSubCategoryFeedsFragment(subcat, feedItemModel.getDescription());
                        }
                    });

                    holder.tagsLinearLayout.addView(view);
                    itemTextView1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (((MainActivity) context).subCategoryFeedsFragment == null)
                                launchSubCategoryFeedsFragment(subcat, feedItemModel.getDescription());
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        double distance = Util.distance(context, feedItemModel.getLat(), feedItemModel.getLng());

        distance = distance / 1000;

        if (distance > 100) {
            holder.distance.setText(" ~5" + " km away");
        } else {
            holder.distance.setText(String.format("%.2f", distance) + " km away");
        }
        holder.progressText.setText(String.format("%.0f", distance));
        holder.distanceProgress.setProgress((int) distance);

        String imageURL1 = "", imageURL2 = "", imageURL3 = "";
        try {
            imageURL1 = feedItemModel.getImageURLs().get(0).toString();
            imageURL2 = feedItemModel.getImageURLs().get(1).toString();
            imageURL3 = feedItemModel.getImageURLs().get(2).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (likedFeeds.contains(String.valueOf(feedItemModel.getId()))) {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_filled));
        } else {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart));
        }

        holder.likes.setText(feedItemModel.getLikes());

        holder.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (likedFeeds.contains(String.valueOf(feedItemModel.getId()))) {
                    Util.showSnackBar("Feed Already Liked", holder.title);
                    return;
                }

                likedFeeds = likedFeeds + "," + feedItemModel.getId();
                ((MainActivity) context).upVoteFeed(feedItemModel.getId(), position);

                holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_filled));
                int like = Integer.parseInt(holder.likes.getText().toString());
                holder.likes.setText(String.valueOf(++like));

                JSONObject postDataParams = new JSONObject();
                Location location = Util.getLocation(context);

                try {
                    postDataParams.put("feedId", feedItemModel.getId());
                    postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    postDataParams.put("gpsLatitude", location.getLatitude());
                    postDataParams.put("gpsLongitude", location.getLongitude());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "UpVote",
                        Constants.UPVOTE_FEED, postDataParams, "POST");

                performAsyncTask.execute();
            }
        });

        if (((MainActivity) context).userFollowedBizIds.contains(feedItemModel.getBizId())) {
            ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_filled));
            ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_hollow));
            ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(android.R.color.darker_gray));
        }

        if (!isFavouriteFragment) {
            if (((MainActivity) context).userFavouritedFeeds.contains(feedItemModel.getId())) {
                holder.favImage.setVisibility(View.GONE);
            } else {
                holder.favImage.setVisibility(View.VISIBLE);
            }
        }

        holder.followLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (feedItemModel.getBizId() == 0) {
                    Util.showSnackBar("Shop Info not available, please try again later", holder.followLayout);
                    return;
                }

                JSONObject postDataParams = new JSONObject();
                try {
                    postDataParams.put("userId", ((MainActivity) context).prefs.getString(Constants.USER_ID, ""));
                    postDataParams.put("bizId", feedItemModel.getBizId());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (((MainActivity) context).userFollowedBizIds.contains(feedItemModel.getBizId())) {
                    ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_hollow));
                    ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(android.R.color.darker_gray));

                    ((MainActivity) context).userFollowedBizIds.remove(new Integer(feedItemModel.bizId));


                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "UnFollowBiz",
                            Constants.DELETE_FAVOURITE_BIZ_INFO, postDataParams, "POST");

                    performAsyncTask.execute();

                    return;
                }


                ((MainActivity) context).userFollowedBizIds.add(feedItemModel.bizId);

                ((ImageView) holder.followLayout.findViewById(R.id.follow_star)).setImageDrawable(context.getResources().getDrawable(R.drawable.follow_star_filled));
                ((TextView) holder.followLayout.findViewById(R.id.follow_text)).setTextColor(context.getResources().getColor(R.color.colorPrimary));

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "FollowBiz",
                        Constants.INSERT_FAVOURITE_BIZ_INFO, postDataParams, "POST");

                performAsyncTask.execute();

            }
        });

        final String shopAreaForDetailFragment = extractedShopArea;

        holder.shopDetailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedItemModel.getBizId() == 0) {
                    //Util.showSnackBar("There is no  shop info available", ((MainActivity) context).recyclerView);
                } else {
                    /*if (((MainActivity) context).bizInfoFragment == null)
                        ((MainActivity) context).launchBizDetailFragment(feedItemModel.getBizId());*/

                    if (((MainActivity) context).bizFeedsFragment == null)
                        ((MainActivity) context).launchBizFeedsFragment(feedItemModel.getBizId(), shopAreaForDetailFragment, feedItemModel.getShopName());
                }
            }
        });

        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Util.showSnackBar("Please grant location permission", ((MainActivity) context).recyclerView);
                    ActivityCompat.requestPermissions((MainActivity) context, new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.CALL_PHONE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);
                    return;
                }

                if (((MainActivity) context).bizInfoFragment == null) {
                    if (feedItemModel.getBizId() > 0)
                        ((MainActivity) context).launchBizDetailFragment(WordUtils.capitalize(feedItemModel.getShopName()), holder.shopArea.getText().toString(), feedItemModel.getBizId(), 0, 0);
                    else
                        ((MainActivity) context).launchBizDetailFragment(feedItemModel.getShopName(), holder.shopArea.getText().toString(), 0, feedItemModel.getLat(), feedItemModel.getLng());
                }

            }
        });


        //holder.shopImage.setImageDrawable(context.getResources().getDrawable(getShopImage(feedItemModel.getMainCategory())));

        ArrayList<String> images = new ArrayList<>();
        images.clear();

        if (imageURL1.length() != 0 && !imageURL1.equals("") && !imageURL1.equals("null")) {
            images.add(imageURL1);
            //images.add("http://lorempixel.com/300/300/cats/1/");
        }
        if (imageURL2.length() != 0 && !imageURL2.equals("") && !imageURL2.equals("null")) {
            images.add(imageURL2);
            //images.add("http://lorempixel.com/300/300/cats/1/");
        }
        if (imageURL3.length() != 0 && !imageURL3.equals("") && !imageURL3.equals("null")) {
            images.add(imageURL3);
            //images.add("http://lorempixel.com/300/300/cats/1/");
        }

        if (images.size() == 1) {
            holder.pager_indicator.setVisibility(View.GONE);
        } else {
            holder.pager_indicator.setVisibility(View.VISIBLE);
        }

        int colorIndex = position % 10;

        holder.mAdapter = new ViewPagerAdapterForFeeds(holder.heart, holder.likes, feedItemModel, context, images, holder.progressBar,
                holder.imageFrame, progressBarColors.get(colorIndex), imageFrameColors.get(colorIndex));
        holder.viewPager.setAdapter(holder.mAdapter);
        //holder.viewPager.setCurrentItem(0);
        setUiPageViewController(holder);

        holder.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                holder.dotsCount = holder.mAdapter.getCount();
                for (int i = 0; i < holder.dotsCount; i++) {
                    holder.dots[i].setImageDrawable(context.getResources().getDrawable(R.drawable.nonselecteditem_dot));
                }

                holder.dots[position].setImageDrawable(context.getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state != ViewPager.SCROLL_STATE_IDLE) {
                    final int childCount = holder.viewPager.getChildCount();
                    for (int i = 0; i < childCount; i++)
                        holder.viewPager.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }
        });

        setAnimation(holder.mainCard, position);
    }

    private void setUiPageViewController(ViewHolder holder) {

        holder.dotsCount = holder.mAdapter.getCount();
        holder.dots = new ImageView[holder.dotsCount];
        holder.pager_indicator.removeAllViews();

        for (int i = 0; i < holder.dotsCount; i++) {
            holder.dots[i] = new ImageView(context);
            holder.dots[i].setImageDrawable(context.getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            holder.pager_indicator.addView(holder.dots[i], params);
        }
        if (holder.dots.length != 0)
            holder.dots[0].setImageDrawable(context.getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public int getItemCount() {
        return feedItemModelArrayList.size();
    }

}

class ViewPagerAdapterForFeeds extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> mResources;
    ProgressBar progressBar;
    FrameLayout imageFrame;
    String progressColor;
    String imageFrameColor;
    FeedItemModel feedItemModel;
    ImageView hearts;
    TextView likes;

    public ViewPagerAdapterForFeeds(ImageView hearts, TextView likes, FeedItemModel feedItemModel, Context mContext, ArrayList<String> mResources, ProgressBar progressBar,
                                    FrameLayout imageFrame, String progressColor, String imageFrameColor) {
        this.hearts = hearts;
        this.likes = likes;
        this.mContext = mContext;
        this.mResources = mResources;
        this.progressBar = progressBar;
        this.imageFrame = imageFrame;
        this.progressColor = progressColor;
        this.imageFrameColor = imageFrameColor;
        this.feedItemModel = feedItemModel;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);

        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor(progressColor), PorterDuff.Mode.SRC_IN);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) mContext).launchFeedDetailFragment(feedItemModel, hearts, likes, position);
            }
        });

        imageFrame.setBackgroundColor(Color.parseColor(imageFrameColor));

        Picasso.with(mContext)
                .load(Constants.ROOT_URL + mResources.get(position))
                .resize(300, 300)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .error(R.drawable.image_place_holder)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                        imageFrame.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                    }

                    @Override
                    public void onError() {

                    }
                });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}