package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by vibinvarghese on 14/12/16.
 */

public class RateFeedDialog extends DialogFragment {

    int feedId;

    public RateFeedDialog() {
    }

    @SuppressLint("ValidFragment")
    public RateFeedDialog(int feedId) {
        this.feedId = feedId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.rate_feed_dialog, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        final Drawable d = new ColorDrawable(Color.TRANSPARENT);
        getDialog().getWindow().setBackgroundDrawable(d);

        final RatingBar rating = (RatingBar) rootView.findViewById(R.id.review_bar);

        final Button send = (Button) rootView.findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int ratingValue = (int) rating.getRating();
                if (ratingValue == 0) {
                    Util.showSnackBar("Please select a rating", send);
                    return;
                }

                insertFeedRating(ratingValue);

                dismiss();
            }
        });

        return rootView;
    }

    protected void insertFeedRating(int rating) {


        JSONObject postDataParams = new JSONObject();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());


        try {
            postDataParams.put("feedId", feedId);
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));
            postDataParams.put("userRating", rating);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "InsertFeedRating",
                Constants.RATE_FEED, postDataParams, "POST");

        performAsyncTask.execute();

    }

}
