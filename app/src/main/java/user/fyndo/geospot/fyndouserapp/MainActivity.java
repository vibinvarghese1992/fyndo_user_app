package user.fyndo.geospot.fyndouserapp;

import android.Manifest;
import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static user.fyndo.geospot.fyndouserapp.R.id.imageView;
import static user.fyndo.geospot.fyndouserapp.R.id.share;

public class MainActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.OnConnectionFailedListener {

    SharedPreferences prefs;
    Context context;
    Spinner shopArea;
    ProgressBar progressBar;
    RelativeLayout progressLayout;
    FloatingActionButton fab;
    RecyclerView recyclerView;
    TextView noFeeds;
    FeedDetailFragment feedDetailFragment;
    TextView selectedCategoriesText;

    ArrayList<String> shopCategoryStore;
    ArrayList<String> shopCategoryEatery;

    ArrayList<FeedItemModel> feedArrayList;
    FeedListRecyclerAdapter feedListRecyclerAdapter;

    TextView selectedCategories;

    ImageView optionsMenu, grid;

    boolean isFirstLaunch = true;

    protected LocationManager locationManager;
    Location location = null;

    CardView bottomTab;

    int currentFeedCount = 20;
    int incrementFeedCount = 5;
    int dynamicFeed = 0;
    int dynamicFeedForSubCategory = 0;
    int dynamicFeedForFavourites = 0;

    JSONArray userCategories = new JSONArray();

    JSONArray currentSelected = new JSONArray();

    BizInfoFragment bizInfoFragment;
    BizFeedsFragment bizFeedsFragment;

    LoginFragment loginFragment;

    ImageView selectedMenIcon, selectedWomenIcon;
    ProfileFragment profileFragment;

    ActionBar actionBar;

    CategorySelectorFragment categorySelectorFragment;
    SubCategorySelectionFragment subCategorySelectionFragment;
    FavouriteFeedsFragment favouriteFeedsFragment;
    SubCategoryStoreFragment subCategoryStoreFragment;
    FavouriteShopsFragment favouriteShopsFragment;

    FavouriteShopFeedsFragment favouriteShopFeedsFragment;

    final String[] titleArray = {"Men", "Women", "Baby & Kids", "Home & Furnitures",
            "Home Appliances", "Consumer Electronics"};

    ArrayList<String> men = new ArrayList<>();
    ArrayList<String> women = new ArrayList<>();
    ArrayList<String> consumerElectronics = new ArrayList<>();
    ArrayList<String> homeAppliances = new ArrayList<>();
    ArrayList<String> homeFurnitures = new ArrayList<>();
    ArrayList<String> babyNKids = new ArrayList<>();


    ArrayList<String> selectedMen = new ArrayList<>();
    ArrayList<String> selectedWomen = new ArrayList<>();
    ArrayList<String> selectedConsumerElectronics = new ArrayList<>();
    ArrayList<String> selectedHomeAppliances = new ArrayList<>();
    ArrayList<String> selectedHomeFurnitures = new ArrayList<>();
    ArrayList<String> selectedBabyNKids = new ArrayList<>();

    SwipeRefreshLayout swipeRefreshLayout;

    boolean previousRequestType = true;

    SubCategoryFeedsFragment subCategoryFeedsFragment;

    LinearLayout subCategoriesScroll;

    ImageView favouriteIcon, preferenceIcon, favouriteShopFeedsIcon, subCategoryStoreIcon;

    String shareIntentShopName, shareIntentShopArea;

    FeedItemModel isSharedItemModel = null;

    FeedItemModel sharedFeedItem;


    ImageView mainFeedIcon, favFeedsIcon;

    ArrayList<Integer> userFollowedBizIds = new ArrayList<>();
    ArrayList<Integer> userFavouritedFeeds = new ArrayList<>();

    @Override
    protected void onResume() {
        super.onResume();

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("MainActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        context = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        initUI();

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(this, "Popup",
                Constants.POPUP_LINK, null, "GET");

        performAsyncTask.execute();

        men.clear();
        men.add("Casual Shirts");
        men.add("Casual Shoes");
        men.add("Casual Trousers");
        men.add("Formal Shirts");
        men.add("Formal Shoes");
        men.add("Formal Trousers");
        men.add("Indian & Festive Wear");
        men.add("Sandals & Floaters");
        men.add("Shorts");
        men.add("Sports Shoes");
        men.add("T-Shirts");
        men.add("Watches & Wearables");

        women.clear();
        women.add("Boutique & Fashion Jewellery");
        women.add("Flats & Casual Shoes");
        women.add("Handbags, Bags & Wallets");
        women.add("Indian & Fusion Wear");
        women.add("Watches & Wearables");
        women.add("Western Wear");

        consumerElectronics.clear();
        consumerElectronics.add("Camera");
        consumerElectronics.add("Headphones & Headsets");
        consumerElectronics.add("Laptops");
        consumerElectronics.add("Mobiles");
        consumerElectronics.add("Tablets");

        homeAppliances.clear();
        homeAppliances.add("Air Conditioner");
        homeAppliances.add("Home Entertainment");
        homeAppliances.add("Kitchen Appliances");
        homeAppliances.add("Refrigerators");
        homeAppliances.add("Small Home Appliances");
        homeAppliances.add("TVs");
        homeAppliances.add("Washing Machine");

        homeFurnitures.clear();
        homeFurnitures.add("Furniture");
        homeFurnitures.add("Home Decor");
        homeFurnitures.add("Lighting");

        babyNKids.clear();
        babyNKids.add("Boy's Clothing");
        babyNKids.add("Girl's Clothing");
        babyNKids.add("Boy's Footwear");
        babyNKids.add("Girl's Footwear");

        actionBar = getSupportActionBar();
        actionBar.hide();

        if (!prefs.getBoolean(Constants.IS_LOGGED_IN, false)) {
            launchLoginFragment();

        } else {
            if (!prefs.getBoolean(Constants.USER_PREFERENCES_ISSET, false))
                launchSubCategorySelectorFragment();
            else {
                makeCustomRequestToPopulateAllFeeds(true, true);
            }
            //launchCategorySelectorFragment();
            //postLogin();
        }

        // --- Deep Linking --------

        // Build GoogleApiClient with AppInvite API for receiving deep links
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(AppInvite.API)
                .build();

        // Check if this app was launched from a deep link. Setting autoLaunchDeepLink to true
        // would automatically launch the deep link if one is found.
        boolean autoLaunchDeepLink = false;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
                .setResultCallback(
                        new ResultCallback<AppInviteInvitationResult>() {
                            @Override
                            public void onResult(@NonNull AppInviteInvitationResult result) {
                                if (result.getStatus().isSuccess()) {
                                    // Extract deep link from Intent
                                    Intent intent = result.getInvitationIntent();
                                    String deepLink = AppInviteReferral.getDeepLink(intent);

                                    String feedId = Uri.parse(intent.getData().toString()).getQueryParameter("feedId");

                                    PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "DeepLinkFeed",
                                            Constants.SELECT_FEED + feedId, null, "GET");

                                    performAsyncTask.execute();

                                } else {
                                    Log.d("Deep Linking", "getInvitation: no deep link found.");
                                }
                            }
                        });

        //----------------------------

        ImageView share = (ImageView) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIntent();
            }
        });

        GoogleAnalytics.getInstance(this).setAppOptOut(true);
    }


    protected FeedItemModel processDeepLinkServerResponse(String response) throws JSONException {

        JSONObject feed = new JSONObject(response);

        FeedItemModel feedItemModel = new FeedItemModel();
        feedItemModel.setTitle(feed.getString("title"));
        int id = feed.getInt("feedId");

        if (!feedItemModel.getTitle().contains("[")) {
            feedItemModel.setMainTitle(feedItemModel.getTitle());
        }

        feedItemModel.setMainDescription(feed.getString("textContent"));

        feedItemModel.setLat(feed.getDouble("gpsLatitude"));
        feedItemModel.setLng(feed.getDouble("gpsLongitude"));
        feedItemModel.setMainCategory(feed.getString("sourceDevice"));

        feedItemModel.setId(id);
        feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
        feedItemModel.setDescription(feed.getString("sourceDevice"));
        feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
        feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
        feedItemModel.setDealType(feed.getString("feedType"));
        String validity = "1";
        if (feed.getInt("numExpiryDays") > 0) {
            validity = String.valueOf(feed.getInt("numExpiryDays"));
        }
        feedItemModel.setValidity(validity);

        feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));

        feedItemModel.setShopName("");
        feedItemModel.setShopImage("");
        feedItemModel.setShopArea("");
        feedItemModel.setBizId(feed.getInt("bizId"));

        if (!feed.isNull("shopName"))
            feedItemModel.setShopName(feed.getString("shopName"));

        if (!feed.isNull("shopArea"))
            feedItemModel.setShopArea(feed.getString("shopArea"));

        String[] areaArray = feedItemModel.getShopArea().split(", ");
        String extractedShopArea = feedItemModel.getShopArea();
        for (int k = 0; k < areaArray.length; k++) {
            if (areaArray[k].equals("Chennai")) {
                extractedShopArea = areaArray[k - 1];
                break;
            }
        }

        feedItemModel.setShopArea(extractedShopArea);

        return feedItemModel;
    }

    protected void launchServerPopup(String response, boolean isSuccess) {
        if (!isSuccess) {
            return;
        }

        JSONArray responseArray;
        JSONObject responseObject = new JSONObject();
        int popupId = 0;
        String appVersion = "";
        try {
            responseArray = new JSONArray(response);
            if (response.length() > 0) {
                responseObject = responseArray.getJSONObject(0);
            } else {
                return;
            }
            popupId = responseObject.getInt("notificationId");
            appVersion = responseObject.getString("targetAppVersion");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        appVersion = appVersion.split("\\+")[0];
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = "";
        if (pInfo != null)
            version = pInfo.versionName;

        int storedInt = prefs.getInt(Constants.LAST_SEEN_POPUP_ID, 0);

        if (storedInt < popupId && version.equals(appVersion)) {
            prefs.edit().putInt(Constants.LAST_SEEN_POPUP_ID, popupId).apply();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ServerNotificationDialog serverNotificationDialog = new ServerNotificationDialog(response);
            serverNotificationDialog.show(ft, "Server Popup");
        }
    }

    protected void launchDeepLinkView(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Please Check you internet connection  and try again", recyclerView);
            return;
        }

        FeedItemModel feedItemModel = null;
        try {
            feedItemModel = processDeepLinkServerResponse(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        isSharedItemModel = feedItemModel;
        launchFeedDetailFragment(feedItemModel, null, null, -1);
    }

    protected void launchSubCategorySelectorFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        subCategorySelectionFragment = new SubCategorySelectionFragment();
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, subCategorySelectionFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void launchSubCategoryFeedsFragment(String subCategory, String mainCategory) {
        dynamicFeedForSubCategory = 0;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        subCategoryFeedsFragment = new SubCategoryFeedsFragment(subCategory, mainCategory);
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, subCategoryFeedsFragment);
        fragmentTransaction.commit();
    }

    protected void launchCategorySelectorFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        categorySelectorFragment = new CategorySelectorFragment();
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, categorySelectorFragment);
        fragmentTransaction.commit();
    }

    protected void setSelectedCategoriesText() {
        String textToSet = "";
        for (int i = 0; i < currentSelected.length(); i++) {
            try {
                if (i == currentSelected.length() - 1)
                    textToSet += currentSelected.getString(i);
                else
                    textToSet += currentSelected.getString(i) + ", ";
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        TextView textView;

        subCategoriesScroll.removeAllViews();

        ArrayList<ArrayList> allSubcategories = new ArrayList<>();
        allSubcategories.add(selectedMen);
        allSubcategories.add(selectedWomen);
        allSubcategories.add(selectedConsumerElectronics);
        allSubcategories.add(selectedHomeAppliances);
        allSubcategories.add(selectedHomeFurnitures);
        allSubcategories.add(selectedBabyNKids);

        Typeface bold = Typeface.createFromAsset(context.getAssets(), "fonts/bold.ttf");

        for (int j = 0; j < allSubcategories.size(); j++) {
            for (int i = 0; i < allSubcategories.get(j).size(); i++) {
                view = (View) inflater.inflate(R.layout.single_item_textview_sub_categories, null);
                textView = (TextView) view.findViewById(R.id.item1);
                textView.setText(allSubcategories.get(j).get(i).toString());
                textView.setTypeface(bold);
                subCategoriesScroll.addView(view);
            }
        }

        if (selectedMen.size() > 0) {
            //selectedMenIcon.setVisibility(View.VISIBLE);
        } else {
            //selectedMenIcon.setVisibility(View.GONE);
        }

        if (selectedWomen.size() > 0) {
            //selectedWomenIcon.setVisibility(View.VISIBLE);
        } else {
            //selectedWomenIcon.setVisibility(View.GONE);
        }


        //selectedCategoriesText.setText(textToSet);
    }

    protected void postUserExistsCheck(String response, boolean isSuccess) {
        if (!isSuccess) {
            loginFragment.createNewUser();
            return;
        }
        String userId = "";
        try {
            userId = new JSONObject(response).getJSONObject("user").getString("userId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        prefs.edit().putString(Constants.USER_ID, userId).apply();

        if (loginFragment != null)
            loginFragment.postLogin();
        else {
            prefs.edit().putBoolean(Constants.IS_LOGGED_IN, true).apply();
            postLogin();
        }
    }

    protected void populateSubCategoryFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, Please try again", recyclerView);
            return;
        }
        try {
            if (subCategoryFeedsFragment != null)
                subCategoryFeedsFragment.populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateSubCategoryStores(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, Please try again", favouriteIcon);
            return;
        }
        try {
            if (subCategoryStoreFragment != null)
                subCategoryStoreFragment.populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFavouriteFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, Please try again", recyclerView);
            return;
        }
        try {
            if (favouriteFeedsFragment != null)
                favouriteFeedsFragment.populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFavouriteShopFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, Please try again", recyclerView);
            return;
        }
        try {
            if (favouriteShopFeedsFragment != null)
                favouriteShopFeedsFragment.populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void postUserNewUserCreated(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Please Check you internet connection  and try again", loginFragment.logo);
            return;
        }

        prefs.edit().putString(Constants.USER_ID, response).apply();
        loginFragment.postLogin();
    }

    protected void launchProfileFragment() {
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        profileFragment = new ProfileFragment();
        fragmentTransaction.replace(android.R.id.content, profileFragment);
        fragmentTransaction.commit();
    }

    protected void launchLoginFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        loginFragment = new LoginFragment();
        fragmentTransaction.replace(android.R.id.content, loginFragment);
        fragmentTransaction.commit();
    }

    protected void postLogin() {

        ActivityCompat.requestPermissions(this, new String[]{
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.CALL_PHONE,
                android.Manifest.permission.ACCESS_FINE_LOCATION}, 111);

        if (loginFragment != null) {

            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(loginFragment).commitAllowingStateLoss();
            loginFragment = null;
        }

        launchSubCategorySelectorFragment();
        //launchCategorySelectorFragment();

        //populateCategoryView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 111) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            //All location services are disabled
                            Util.showSnackBar("Please enable your location/gps", recyclerView);
                        } else {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                            makeCustomRequestToPopulateAllFeeds(false, true);
                            Location location = Util.getLocation(context);
                        }
                    } else {
                        Util.showSnackBar("Please grant location Permissions to use the app", recyclerView);
                    }
                }
            }
        }
    }


    protected void makeCustomRequestToPopulateAllFeeds(boolean freshLoad, boolean isSubCategoryFilterFromLaunch) {
        if (freshLoad) {
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {

                                  if (!swipeRefreshLayout.isRefreshing())
                                      progressLayout.setVisibility(View.VISIBLE);
                                  //progressBar.setVisibility(View.VISIBLE);
                                  recyclerView.setVisibility(View.GONE);
                                  recyclerView.scrollToPosition(0);
                              }
                          }

            );
        }

        previousRequestType = isSubCategoryFilterFromLaunch;

        JSONObject postDataParams = new JSONObject();

        //userCategories = new JSONArray();
        if (currentSelected.length() == 0) {
            //userCategories = new JSONArray();
            //userCategories.put("All");
        } else {
            userCategories = currentSelected;
        }

        JSONArray filterCategories = new JSONArray();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String json;
        JSONArray level2Cat;
        JSONObject subCategoryObject;
        Gson gson = new Gson();

        JSONArray selectedCategories = new JSONArray();

        if (isSubCategoryFilterFromLaunch) {
            Type type = new TypeToken<List<String>>() {
            }.getType();

            try {

                json = prefs.getString(Constants.USER_PREFERENCES_MEN, null);
                if (prefs.getString(Constants.USER_PREFERENCES_MEN, null) == null && prefs.getString(Constants.USER_PREFERENCES_WOMEN, null) == null)
                    return;

                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Men");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedMen = gson.fromJson(json, type);
                    selectedCategories.put("Men");
                }

                json = prefs.getString(Constants.USER_PREFERENCES_WOMEN, null);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Women");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Women");
                    selectedWomen = gson.fromJson(json, type);
                }

                json = prefs.getString(Constants.USER_PREFERENCES_CONSUMER_ELECTRONICS, null);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Consumer Electronics");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Consumer Electronics");
                    selectedConsumerElectronics = gson.fromJson(json, type);
                }

                json = prefs.getString(Constants.USER_PREFERENCES_HOME_APPLIANCES, null);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Home Appliances");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Home Appliances");
                    selectedHomeAppliances = gson.fromJson(json, type);
                }

                json = prefs.getString(Constants.USER_PREFERENCES_HOME_FURNITURE, null);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Home & Furnitures");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Home & Furnitures");
                    selectedHomeFurnitures = gson.fromJson(json, type);
                }

                json = prefs.getString(Constants.USER_PREFERENCES_BABY_N_KIDS, null);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Baby & Kids");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Baby & Kids");
                    selectedBabyNKids = gson.fromJson(json, type);
                }

                currentSelected = selectedCategories;
                setSelectedCategoriesText();

                if (!(userCategories.length() == 1 && userCategories.get(0).equals("All")))
                    postDataParams.put("filterCategories", filterCategories);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            try {
                json = gson.toJson(selectedMen);
                if (json == null)
                    return;

                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Men");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Men");
                }

                json = gson.toJson(selectedWomen);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Women");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Women");
                }

                json = gson.toJson(selectedConsumerElectronics);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Consumer Electronics");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Consumer Electronics");
                }

                json = gson.toJson(selectedHomeAppliances);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Home Appliances");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Home Appliances");
                }

                json = gson.toJson(selectedHomeFurnitures);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Home & Furnitures");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Home & Furnitures");
                }

                json = gson.toJson(selectedBabyNKids);
                level2Cat = new JSONArray(json);
                subCategoryObject = new JSONObject();
                if (level2Cat.length() > 0) {
                    subCategoryObject.put("userCatLevel1", "Baby & Kids");
                    subCategoryObject.put("userCatLevel2", level2Cat);
                    filterCategories.put(subCategoryObject);
                    selectedCategories.put("Baby & Kids");
                }

                currentSelected = selectedCategories;
                setSelectedCategoriesText();

                if (!(userCategories.length() == 1 && userCategories.get(0).equals("All")))
                    postDataParams.put("filterCategories", filterCategories);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        Location location = Util.getLocation(this);

        try {
            postDataParams.put("userCategories", userCategories);
            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
            postDataParams.put("dynamicFeedId", dynamicFeed);
            postDataParams.put("noOfFeeds", currentFeedCount);
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(this, "GetCustomFeeds",
                Constants.SELECT_DYNAMIC_FEEDS, postDataParams, "POST");

        performAsyncTask.execute();
    }

    protected void upVoteFeed(int id, int position) {
        String likedFeeds = prefs.getString(Constants.LIKED_FEED_IDS, "");
        position = feedArrayList.size() - position - 1;
        String likes = feedArrayList.get(position).getLikes();
        if (likes == null || likes.equals(""))
            likes = "0";
        int incrLikes = Integer.parseInt(likes);
        feedArrayList.get(position).setLikes(String.valueOf(++incrLikes));
        prefs.edit().putString(Constants.LIKED_FEED_IDS, likedFeeds + "," + id).apply();
    }

    protected void launchFeedDetailFragment(FeedItemModel feedItemModel, ImageView heart, TextView likes, int position) {

        bottomTab.setVisibility(View.GONE);
        Location location = Util.getLocation(context);
        JSONObject postDataParams = new JSONObject();
        try {
            postDataParams.put("feedId", feedItemModel.getId());
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));
            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "IncrementFeedDetailCount",
                Constants.OPEN_FEED, postDataParams, "POST");

        performAsyncTask.execute();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        feedDetailFragment = new FeedDetailFragment(feedItemModel, heart, likes, position);
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, feedDetailFragment);
        fragmentTransaction.commit();
    }

    protected void launchBizFeedsFragment(int bizId, String shopArea, String shopName) {

        bottomTab.setVisibility(View.GONE);

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "IncrementBizOpenCount",
                Constants.INCREMENT_BIZ_CLICK + bizId, null, "GET");

        performAsyncTask.execute();


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bizFeedsFragment = new BizFeedsFragment(bizId, shopArea, shopName);
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, bizFeedsFragment);
        fragmentTransaction.commit();
    }

    protected void populateFeeds(String response, boolean isSuccess) {

        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", recyclerView);
            progressLayout.setVisibility(View.GONE);
            return;
        }

        try {
            populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void shareFeed(String response, boolean isSuccess) {

        if (!isSuccess) {
            return;
        }

        JSONObject responseObject = null;
        String shareUrl = "";
        try {
            responseObject = new JSONObject(response);
            shareUrl = responseObject.getString("shortLink");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray formatted = new JSONArray();
        for (int i = 0; i < sharedFeedItem.getImageURLs().length(); i++) {
            try {
                formatted.put("http://fyndox.herokuapp.com/FeedImagesOrig" + (sharedFeedItem.getImageURLs().get(i).toString().replace("/FeedImages", "")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        sharedFeedItem.setImageURLs(formatted);

        String shareBody = "Hey, " + shareIntentShopName.replace("@", "") + "(" + shareIntentShopArea + ")" + " has an interesting " +
                "promotion in store. Check it out on Fyndo at " + shareUrl;

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Fyndo with your friends ");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share Using"));
    }

    protected void drawMapDirection(String response, boolean isSuccess) {

        if (!isSuccess) {
            return;
        }

        if (bizInfoFragment != null) {
            bizInfoFragment.parseMapDirectionResponse(response);
        }
    }

    protected void launchBizDetailFragment(String shopName, String shopArea, int bizId, double lat, double lng) {
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        bizInfoFragment = new BizInfoFragment(shopArea, shopName, bizId, lat, lng);
        fragmentTransaction.setCustomAnimations(R.anim.bottom_to_center, R.anim.bottom_to_center);
        fragmentTransaction.add(android.R.id.content, bizInfoFragment);
        fragmentTransaction.commit();
    }

    protected void populateBizInfo(String response, boolean isSuccess) {
        if (bizInfoFragment != null)
            bizInfoFragment.populateBizData(response, isSuccess);
    }

    protected void populateBizFeeds(String response, boolean isSuccess) {
        if (bizFeedsFragment != null)
            bizFeedsFragment.populateBizFeeds(response, isSuccess);
    }

    protected void populateCustomFeeds(String response, boolean isSuccess) {

        swipeRefreshLayout.setRefreshing(false);

        if (!isSuccess) {
            Util.showSnackBar("Something went wrong. Please try again.", recyclerView);
            progressLayout.setVisibility(View.GONE);
            return;
        }

        try {
            JSONObject responseObject = new JSONObject(response);

            final JSONArray feedsArray = responseObject.getJSONArray("feeds");
            if (feedsArray.length() > 0 || dynamicFeed != 0)
                populateFeedsArray(response);
            else {

                currentSelected = new JSONArray();
                currentFeedCount = 20;
                dynamicFeed = 0;
                feedArrayList.clear();
                feedListRecyclerAdapter.notifyDataSetChanged();
                userCategories.put("All");

                currentSelected = new JSONArray();
                currentSelected.put("Men");
                currentSelected.put("Women");
                currentSelected.put("Consumer Electronics");
                currentSelected.put("Home Appliances");
                currentSelected.put("Home & Furnitures");
                currentSelected.put("Baby & Kids");

                setSelectedCategoriesText();

                //populateCategoryView();
                makeCustomRequestToPopulateAllFeeds(true, true);

                launchNoFeedsDialog();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    protected void populateFeedsArray(String response) throws JSONException {

        JSONObject responseObject = new JSONObject(response);

        JSONObject userObject = responseObject.getJSONObject("fyndoUser");

        JSONArray followedBizInfos = userObject.getJSONArray("favouriteBizInfos");
        JSONArray favouriteFeeds = userObject.getJSONArray("favouriteFeeds");

        Type type = new TypeToken<List<Integer>>() {
        }.getType();

        Gson gson = new Gson();

        userFavouritedFeeds = gson.fromJson(String.valueOf(favouriteFeeds), type);
        userFollowedBizIds = gson.fromJson(String.valueOf(followedBizInfos), type);

        final JSONArray feedsArray = responseObject.getJSONArray("feeds");


        if (feedArrayList.size() > 0) {
            feedArrayList.remove(feedArrayList.size() - 1);
        }

        //feedArrayList.clear();

        for (int i = 0; i < feedsArray.length(); i++) {

            JSONObject feed = feedsArray.getJSONObject(i);
            if (feed.isNull("linkedFiles"))
                continue;

            FeedItemModel feedItemModel = new FeedItemModel();
            feedItemModel.setTitle(feed.getString("title"));
            int id = feed.getInt("feedId");

            if (!feedItemModel.getTitle().contains("[")) {
                feedItemModel.setMainTitle(feedItemModel.getTitle());
            }


            if (!feed.isNull("bizInfo")) {
                feedItemModel.setShopImage(feed.getJSONObject("bizInfo").getString("bizProfile"));
            } else {
                feedItemModel.setShopImage("");
            }

            feedItemModel.setRating((int) feed.getDouble("userRating"));

            feedItemModel.setMainDescription(feed.getString("textContent"));

            feedItemModel.setLat(feed.getDouble("gpsLatitude"));
            feedItemModel.setLng(feed.getDouble("gpsLongitude"));
            feedItemModel.setMainCategory(feed.getString("sourceDevice"));

            feedItemModel.setId(id);
            feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
            feedItemModel.setDescription(feed.getString("sourceDevice"));
            feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
            feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
            feedItemModel.setDealType(feed.getString("feedType"));
            String validity = "1";
            if (feed.getInt("numExpiryDays") > 0) {
                validity = String.valueOf(feed.getInt("numExpiryDays"));
            }
            feedItemModel.setValidity(validity);

            feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));

            feedItemModel.setShopName("");
            feedItemModel.setShopArea("");
            feedItemModel.setBizId(feed.getInt("bizId"));

            if (!feed.isNull("shopName"))
                feedItemModel.setShopName(feed.getString("shopName"));

            if (!feed.isNull("shopArea"))
                feedItemModel.setShopArea(feed.getString("shopArea"));

            feedArrayList.add(feedItemModel);
        }

        /*long seed = System.nanoTime();
        Collections.shuffle(feedArrayList, new Random(seed));*/

        FeedItemModel lastItem = new FeedItemModel();
        lastItem.setTitle("Last Item");
        feedArrayList.add(lastItem);

        runOnUiThread(new Runnable() {
                          @Override
                          public void run() {

                              if (feedsArray.length() < 20) {
                                  feedArrayList.remove(feedArrayList.size() - 1);
                                  feedListRecyclerAdapter.notifyDataSetChanged();
                                  //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());
                                  feedListRecyclerAdapter.continueRequests = false;
                              } else {
                                  feedListRecyclerAdapter.notifyDataSetChanged();
                                  //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());
                                  feedListRecyclerAdapter.continueRequests = true;
                              }

                              progressLayout.setVisibility(View.GONE);
                              recyclerView.setVisibility(View.VISIBLE);
                              feedListRecyclerAdapter.notifyDataSetChanged();
                              //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());

                              if (feedArrayList.size() == 0) {
                                  noFeeds.setVisibility(View.VISIBLE);
                              } else {
                                  noFeeds.setVisibility(View.GONE);
                              }
                          }
                      }
        );


    }

    protected void cycleColorOfBottomTabIcons(ImageView clickedView) {
        mainFeedIcon.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        favouriteIcon.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        favouriteShopFeedsIcon.setColorFilter(getResources().getColor(android.R.color.darker_gray));
        subCategoryStoreIcon.setColorFilter(getResources().getColor(android.R.color.darker_gray));


        clickedView.setColorFilter(getResources().getColor(R.color.colorPrimary));

        if (clickedView.getId() == R.id.main_feeds)
            bottomTab.setVisibility(View.VISIBLE);

    }

    protected void initUI() {
        ///shopArea = (Spinner) findViewById(R.id.shop_area);
        //progressBar = (ProgressBar) findViewById(R.id.progress);
        progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));

        subCategoriesScroll = (LinearLayout) findViewById(R.id.row3);

        mainFeedIcon = (ImageView) findViewById(R.id.main_feeds);
        mainFeedIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cycleColorOfBottomTabIcons(mainFeedIcon);


                if (favouriteShopFeedsFragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .remove(favouriteShopFeedsFragment).commit();
                    favouriteShopFeedsFragment = null;
                }

                if (subCategoryStoreFragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .remove(subCategoryStoreFragment).commit();
                    subCategoryStoreFragment = null;
                }

                if (favouriteFeedsFragment != null) {
                    getSupportFragmentManager().beginTransaction()
                            .remove(favouriteFeedsFragment).commit();
                    favouriteFeedsFragment = null;
                }

                onBackPressed();
            }
        });

        favouriteIcon = (ImageView) findViewById(R.id.fav_feeds);
        favouriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cycleColorOfBottomTabIcons(favouriteIcon);
                launchFavouritesFragment();

            }
        });


        favouriteShopFeedsIcon = (ImageView) findViewById(R.id.favourite_shop_feeds);
        favouriteShopFeedsIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cycleColorOfBottomTabIcons(favouriteShopFeedsIcon);
                launchFavouritesByShopFragment();
            }
        });


        subCategoryStoreIcon = (ImageView) findViewById(R.id.sub_cat_store);
        subCategoryStoreIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cycleColorOfBottomTabIcons(subCategoryStoreIcon);
                launchSubCategoryStoreFragment();
            }
        });


        ImageView feedback = (ImageView) findViewById(R.id.feed_back);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                launchFeedbackDialog();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentFeedCount = 20;
                dynamicFeed = 0;
                feedArrayList.clear();
                feedListRecyclerAdapter.notifyDataSetChanged();

                makeCustomRequestToPopulateAllFeeds(true, previousRequestType);
            }
        });

        selectedCategoriesText = (TextView) findViewById(R.id.selected_categories_text);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        bottomTab = (CardView) findViewById(R.id.bottom_tab);
        fab.setClickable(true);
        optionsMenu = (ImageView) findViewById(R.id.options_menu);
        grid = (ImageView) findViewById(R.id.grid);
        selectedMenIcon = (ImageView) findViewById(R.id.selected_men);
        selectedWomenIcon = (ImageView) findViewById(R.id.selected_women);
        grid = (ImageView) findViewById(R.id.grid);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSelectCategoryDialog();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //selectedCategories = (TextView) findViewById(R.id.selected_categories);
        noFeeds = (TextView) findViewById(R.id.no_feeds);

        optionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(MainActivity.this, optionsMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.settings, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.preferences:
                                launchSubCategorySelectorFragment();
                                return true;
                            case R.id.favourite_shops:
                                launchFavouriteShopsFragment();
                                return true;
                            case R.id.feedback:
                                launchFeedbackDialog();
                                return true;
                        }
                        return true;
                    }

                });

                popup.show();//showing popup menu
            }
        });

        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSubCategoryFilterDialog();
            }
        });

        feedArrayList = new ArrayList<>();

        feedListRecyclerAdapter = new FeedListRecyclerAdapter(this, feedArrayList, prefs.getString(Constants.LIKED_FEED_IDS, ""), false);

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return 9000;
            }
        };

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedListRecyclerAdapter);

    }

    protected void launchFavouritesFragment() {
        dynamicFeedForFavourites = 0;
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        favouriteFeedsFragment = new FavouriteFeedsFragment();
        fragmentTransaction.replace(R.id.replaceable_content, favouriteFeedsFragment);
        fragmentTransaction.commit();
    }

    protected void launchFavouriteShopsFragment() {
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        favouriteShopsFragment = new FavouriteShopsFragment();
        fragmentTransaction.replace(android.R.id.content, favouriteShopsFragment);
        fragmentTransaction.commit();
    }

    protected void launchSubCategoryStoreFragment() {
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        subCategoryStoreFragment = new SubCategoryStoreFragment();
        fragmentTransaction.replace(R.id.replaceable_content, subCategoryStoreFragment);
        fragmentTransaction.commit();
    }

    protected void launchFavouritesByShopFragment() {
        bottomTab.setVisibility(View.GONE);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        favouriteShopFeedsFragment = new FavouriteShopFeedsFragment();
        fragmentTransaction.replace(R.id.replaceable_content, favouriteShopFeedsFragment);
        fragmentTransaction.commit();
    }

    protected void launchCategoryFilterDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        CategoryFilterDialog categoryFilterDialog = new CategoryFilterDialog();
        categoryFilterDialog.show(ft, "Category Filter Dialog");
    }

    protected void launchSubCategoryFilterDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        SubCategorySelectionDialog subCategorySelectionDialog = new SubCategorySelectionDialog();
        subCategorySelectionDialog.show(ft, "SubCategory Filter Dialog");
    }

    protected void shareIntent() {

        String shareBody = "Hey, start discovering real products and promotions around your area. Download Fyndo from www.fyndo.co now!";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Fyndo with your friends");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share Using"));

    }

    protected void populateShopAreaSpinner(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something went wrong, Please try again", progressLayout);
            return;
        }

        final ArrayList<String> shopAreas = new ArrayList<>();
        JSONArray responseArray = null;
        try {
            responseArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < responseArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = responseArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject == null)
                continue;

            try {
                shopAreas.add(jsonObject.getString("areaName"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shopAreas);
        editor.putString(Constants.BIZ_AREA_LIST, json);
        editor.apply();

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter shopAreaAdapter = new ArrayAdapter(context, R.layout.spinner_item, shopAreas);
                shopAreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                shopArea.setAdapter(shopAreaAdapter);

                shopAreaAdapter.notifyDataSetChanged();
            }
        });

        /*PerformAsyncTask performAsyncTask = new PerformAsyncTask(context, "GetBizCategories",
                Constants.SELECT_BIZ_CATEGORIES, null, "GET");

        performAsyncTask.execute();*/
    }

    protected void saveBizCategories(String response, boolean isSuccess) {
        if (!isSuccess) {
            return;
        }

        shopCategoryStore = new ArrayList<>();
        shopCategoryEatery = new ArrayList<>();
        JSONArray responseArray = null;
        try {
            responseArray = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String type;

        for (int i = 0; i < responseArray.length(); i++) {
            JSONObject jsonObject = null;
            try {
                jsonObject = responseArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonObject == null)
                continue;

            try {
                type = jsonObject.getString("bizType");
                if (type.equals("Eatery")) {
                    shopCategoryEatery.add(jsonObject.getString("bizCategory"));
                } else {
                    shopCategoryStore.add(jsonObject.getString("bizCategory"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                shopArea.setSelection(prefs.getInt(Constants.SELECTED_AREA, 0));
            }
        });

        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(shopCategoryStore);
        editor.putString(Constants.SHOP_CATEGORY_STORE, json);
        json = gson.toJson(shopCategoryEatery);
        editor.putString(Constants.SHOP_CATEGORY_EATERY, json);
        editor.apply();

        if (selectedCategories.getText().equals("Please Select Categories") || prefs.getString(Constants.SELECTED_CATEGORIES_ARRAY, null) == null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noFeeds.setVisibility(View.VISIBLE);
                    progressLayout.setVisibility(View.GONE);
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            launchSelectCategoryDialog();
                        }
                    });
                }
            });
        } else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            launchSelectCategoryDialog();
                        }
                    });
                }
            });

            JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("bizArea", shopArea.getSelectedItem().toString());
                postDataParams.put("bizType", prefs.getString(Constants.SELECTED_BIZ_TYPE, ""));
                postDataParams.put("bizCategories", new JSONArray(prefs.getString(Constants.SELECTED_CATEGORIES_ARRAY, null)));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*PerformAsyncTask performAsyncTask = new PerformAsyncTask(this, "GetFeeds",
                    "http://fyndox.herokuapp.com/api/selectFeeds", postDataParams, "GET");

            performAsyncTask.execute();*/

        }

        /*PerformAsyncTask performAsyncTask = new PerformAsyncTask(this, "GetFeeds",
                "http://fyndox.herokuapp.com/api/selectFeeds", null, "GET");

        performAsyncTask.execute();*/


    }

    protected void launchRateFeedDialog(int feedId) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        RateFeedDialog feedBackDialog = new RateFeedDialog(feedId);
        feedBackDialog.show(ft, "Rating dialog");
    }

    protected void launchNoInternetDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        NoInternetDialog noInternetDialog = new NoInternetDialog();
        noInternetDialog.show(ft, "No Internet dialog");
    }

    protected void launchNoFeedsDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        NoFeedsDialog noFeedsDialog = new NoFeedsDialog();
        noFeedsDialog.show(ft, "No Feeds dialog");
    }

    protected void upVoteStatus(String response, boolean isSuccess) {
        if (isSuccess) {
            //Util.showSnackBar("Upvote Successful", recyclerView);
            return;
        }
    }

    protected void launchSelectCategoryDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        MainCategoryDialogFragment mainCategoryDialogFragment = new MainCategoryDialogFragment(context);
        mainCategoryDialogFragment.show(ft, "Feed Preview Dialog");
    }

    protected void launchFeedbackDialog() {

        AnalyticsApplication application = (AnalyticsApplication) context.getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("FeedBackDialogClicked");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        FeedBackDialog feedBackDialog = new FeedBackDialog();
        feedBackDialog.show(ft, "Feed back");
    }

    protected void checkAndShowTitleBar() {
        if (favouriteShopsFragment == null && subCategoryStoreFragment == null && favouriteShopFeedsFragment == null && favouriteFeedsFragment == null && subCategoryFeedsFragment == null && subCategorySelectionFragment == null && categorySelectorFragment == null && profileFragment == null &&
                feedDetailFragment == null && bizInfoFragment == null && bizFeedsFragment == null) {

            // Obtain the shared Tracker instance.
            AnalyticsApplication application = (AnalyticsApplication) getApplicationContext();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("MainActivity");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

            if (isSharedItemModel != null && !feedArrayList.contains(isSharedItemModel)) {
                feedArrayList.add(0, isSharedItemModel);
                feedListRecyclerAdapter.notifyDataSetChanged();
            }

            bottomTab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {

        if (favouriteShopsFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(favouriteShopsFragment).commit();
            favouriteShopsFragment = null;

            checkAndShowTitleBar();
            return;
        }

        if (subCategorySelectionFragment != null) {
            if (prefs.getString(Constants.USER_PREFERENCES_MEN, null) == null && prefs.getString(Constants.USER_PREFERENCES_WOMEN, null) == null)
                return;

            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(subCategorySelectionFragment).commit();
            subCategorySelectionFragment = null;

            checkAndShowTitleBar();
            return;
        }
        if (categorySelectorFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(categorySelectorFragment).commit();
            categorySelectorFragment = null;

            checkAndShowTitleBar();
            return;
        }

        if (profileFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(profileFragment).commit();
            profileFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (feedDetailFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(feedDetailFragment).commit();
            feedDetailFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (bizInfoFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(bizInfoFragment).commit();
            bizInfoFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (subCategoryFeedsFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(subCategoryFeedsFragment).commit();
            subCategoryFeedsFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (favouriteFeedsFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .remove(favouriteFeedsFragment).commit();
            favouriteFeedsFragment = null;
            checkAndShowTitleBar();
            return;
        }


        if (subCategoryStoreFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .remove(subCategoryStoreFragment).commit();
            subCategoryStoreFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (favouriteShopFeedsFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .remove(favouriteShopFeedsFragment).commit();
            favouriteShopFeedsFragment = null;
            checkAndShowTitleBar();
            return;
        }

        if (bizFeedsFragment != null) {
            this.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.center_to_bottom, R.anim.center_to_bottom)
                    .remove(bizFeedsFragment).commit();
            bizFeedsFragment = null;
            checkAndShowTitleBar();
            return;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if (isFirstLaunch) {
            //makeCustomRequestToPopulateAllFeeds(false);
            isFirstLaunch = false;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
