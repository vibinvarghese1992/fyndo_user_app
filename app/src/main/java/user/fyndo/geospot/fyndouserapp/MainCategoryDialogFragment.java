package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by vibinvarghese on 11/11/16.
 */

public class MainCategoryDialogFragment extends DialogFragment {

    Context context;
    ListView listView;
    CategoryListAdapter categoryListAdapter;
    SharedPreferences prefs;

    @SuppressLint("ValidFragment")
    public MainCategoryDialogFragment(Context context) {
        this.context = context;
    }

    public MainCategoryDialogFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_category_selection_dialog, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        listView = (ListView) rootView.findViewById(R.id.store_list);

        TextView okButton = (TextView) rootView.findViewById(R.id.ok_button);
        TextView cancelButton = (TextView) rootView.findViewById(R.id.cancel_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFeedList();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<String> mainCategories = new ArrayList<>();
        mainCategories.add("Men");
        mainCategories.add("Women");
        mainCategories.add("Consumer Electronics");
        mainCategories.add("Home Appliances");
        mainCategories.add("Home & Furnitures");
        mainCategories.add("Jewellery");
        mainCategories.add("Books & More");
        mainCategories.add("Baby & Kids");
        mainCategories.add("Sports & Fitness");
        mainCategories.add("Automobile & Accessories");

        Gson gson = new Gson();
        String json = prefs.getString(Constants.SELECTED_MAIN_CATEGORIES_ARRAY, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> selectedItems = gson.fromJson(json, type);

        categoryListAdapter = new CategoryListAdapter(getActivity(), mainCategories, selectedItems);
        listView.setAdapter(categoryListAdapter);

    }

    protected void updateFeedList() {
        Gson gson = new Gson();
        String json = gson.toJson(categoryListAdapter.checkedPositions);
        prefs.edit().putString(Constants.SELECTED_MAIN_CATEGORIES_ARRAY, json).apply();

        JSONArray bizCategories = new JSONArray();

        String pos;

        for (int i = 0; i < categoryListAdapter.checkedPositions.size(); i++) {
            pos = categoryListAdapter.checkedPositions.get(i);
            bizCategories.put(categoryListAdapter.listItems.get(Integer.valueOf(pos)));

        }

        ((MainActivity) getActivity()).feedListRecyclerAdapter.lastPosition = -1;
        ((MainActivity) getActivity()).userCategories = bizCategories;

        ((MainActivity) getActivity()).currentFeedCount = 5;

        ((MainActivity) getActivity()).makeCustomRequestToPopulateAllFeeds(true, false);

        dismiss();
    }
}
