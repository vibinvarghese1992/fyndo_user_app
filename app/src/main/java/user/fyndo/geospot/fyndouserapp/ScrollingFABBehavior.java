package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

/**
 * Created by vibinvarghese on 11/11/16.
 */

public class ScrollingFABBehavior extends FloatingActionButton.Behavior {

    Context context;

    private static final String TAG = "ScrollingFABBehavior";

    public ScrollingFABBehavior(Context context, AttributeSet attrs) {
        super();
        this.context = context;
        // Log.e(TAG, "ScrollAwareFABBehavior");
    }


    public boolean onStartNestedScroll(CoordinatorLayout parent, FloatingActionButton child, View directTargetChild, View target, int nestedScrollAxes) {

        return true;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
        if (dependency instanceof RecyclerView)
            return true;

        return false;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout,
                               FloatingActionButton child, View target, int dxConsumed,
                               int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        // TODO Auto-generated method stub

        final CardView text = (CardView) coordinatorLayout.findViewById(R.id.bottom_tab);

        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed,
                dxUnconsumed, dyUnconsumed);

        if (((MainActivity) context).bizFeedsFragment != null)
            return;

        //Log.e(TAG, "onNestedScroll called");
        if (dyConsumed > 0 && text.getVisibility() == View.VISIBLE) {
            //   Log.e(TAG, "child.hide()");

            Animation hideAnimation = AnimationUtils.loadAnimation(context, R.anim.hide_bottom);
            hideAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    text.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            text.startAnimation(hideAnimation);
            //child.hide();
        } else if (dyConsumed < 0 && text.getVisibility() != View.VISIBLE) {
            //  Log.e(TAG, "child.show()");
            //child.show();
            Animation showAnimation = AnimationUtils.loadAnimation(context, R.anim.show_bottom);
            text.startAnimation(showAnimation);
            text.setVisibility(View.VISIBLE);
        }
    }

}
