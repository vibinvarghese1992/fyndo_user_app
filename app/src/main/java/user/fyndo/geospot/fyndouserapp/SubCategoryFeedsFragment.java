package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 23/11/16.
 */

public class SubCategoryFeedsFragment extends Fragment {

    int bizId;
    String subCategory, mainCategory;

    RecyclerView recyclerView;
    ProgressBar progressBar;

    ArrayList<FeedItemModel> feedArrayList = new ArrayList<>();
    FeedListRecyclerAdapter feedListRecyclerAdapter;

    SharedPreferences prefs;

    public SubCategoryFeedsFragment() {
    }

    @SuppressLint("ValidFragment")
    public SubCategoryFeedsFragment(String subCategory, String mainCategory) {
        this.subCategory = subCategory;
        this.mainCategory = mainCategory;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.subcategory_feed_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        ImageView cross = (ImageView) rootView.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).bottomTab.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });


        TextView mainCategoryTextView = (TextView) rootView.findViewById(R.id.main_category);
        TextView subCategoryTextView = (TextView) rootView.findViewById(R.id.sub_category);

        mainCategoryTextView.setText(mainCategory);
        subCategoryTextView.setText(subCategory);


        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("SubCategoryFeedsFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        feedListRecyclerAdapter = new FeedListRecyclerAdapter(getActivity(), feedArrayList, prefs.getString(Constants.LIKED_FEED_IDS, ""), false);
        feedListRecyclerAdapter.notifyDataSetChanged();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedListRecyclerAdapter);


        makeRequest();

        return rootView;
    }

    protected void makeRequest() {
        JSONObject postDataParams = new JSONObject();

        Location location = Util.getLocation(getActivity());

        JSONArray filterCategories = new JSONArray();
        JSONObject filterObject = new JSONObject();
        JSONArray subCategoryArray = new JSONArray();
        try {
            filterObject.put("userCatLevel1", mainCategory);
            subCategoryArray.put(subCategory);
            filterObject.put("userCatLevel2", subCategoryArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        filterCategories.put(filterObject);
        try {
            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
            postDataParams.put("filterCategories", filterCategories);
            postDataParams.put("dynamicFeedId", ((MainActivity) getActivity()).dynamicFeedForSubCategory);
            postDataParams.put("noOfFeeds", ((MainActivity) getActivity()).currentFeedCount);
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("Object", postDataParams.toString());


        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "SubCategoryFeeds",
                Constants.SELECT_DYNAMIC_FEEDS, postDataParams, "POST");

        performAsyncTask.execute();
    }

    protected void populateBizFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something Went wrong, please try again", recyclerView);
            return;
        }

        try {
            populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFeedsArray(String response) throws JSONException {
        JSONObject responseObject = new JSONObject(response);

        final JSONArray feedsArray = responseObject.getJSONArray("feeds");


        if (feedArrayList.size() > 0) {
            feedArrayList.remove(feedArrayList.size() - 1);
        }

        //feedArrayList.clear();

        for (int i = 0; i < feedsArray.length(); i++) {

            JSONObject feed = feedsArray.getJSONObject(i);
            if (feed.isNull("linkedFiles"))
                continue;

            FeedItemModel feedItemModel = new FeedItemModel();
            feedItemModel.setTitle(feed.getString("title"));
            int id = feed.getInt("feedId");

            feedItemModel.setRating((int) feed.getDouble("userRating"));

            if (!feedItemModel.getTitle().contains("[")) {
                feedItemModel.setMainTitle(feedItemModel.getTitle());
            }

            if (!feed.isNull("bizInfo")) {
                feedItemModel.setShopImage(feed.getJSONObject("bizInfo").getString("bizProfile"));
            } else {
                feedItemModel.setShopImage("");
            }

            feedItemModel.setMainDescription(feed.getString("textContent"));

            feedItemModel.setLat(feed.getDouble("gpsLatitude"));
            feedItemModel.setLng(feed.getDouble("gpsLongitude"));
            feedItemModel.setMainCategory(feed.getString("sourceDevice"));

            feedItemModel.setId(id);
            feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
            feedItemModel.setDescription(feed.getString("sourceDevice"));
            feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
            feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
            feedItemModel.setDealType(feed.getString("feedType"));
            String validity = "1";
            if (feed.getInt("numExpiryDays") > 0) {
                validity = String.valueOf(feed.getInt("numExpiryDays"));
            }
            feedItemModel.setValidity(validity);

            feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));

            feedItemModel.setShopName("");
            feedItemModel.setShopArea("");
            feedItemModel.setBizId(feed.getInt("bizId"));

            if (!feed.isNull("shopName"))
                feedItemModel.setShopName(feed.getString("shopName"));

            if (!feed.isNull("shopArea"))
                feedItemModel.setShopArea(feed.getString("shopArea"));

            feedArrayList.add(feedItemModel);
        }

        /*long seed = System.nanoTime();
        Collections.shuffle(feedArrayList, new Random(seed));*/

        FeedItemModel lastItem = new FeedItemModel();
        lastItem.setTitle("Last Item");
        feedArrayList.add(lastItem);

        getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (feedsArray.length() < 20) {
                                                feedArrayList.remove(feedArrayList.size() - 1);
                                                feedListRecyclerAdapter.notifyDataSetChanged();
                                                //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());
                                                feedListRecyclerAdapter.continueRequests = false;
                                            } else {
                                                feedListRecyclerAdapter.notifyDataSetChanged();
                                                //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());
                                                feedListRecyclerAdapter.continueRequests = true;
                                            }

                                            progressBar.setVisibility(View.GONE);
                                            recyclerView.setVisibility(View.VISIBLE);
                                            feedListRecyclerAdapter.notifyDataSetChanged();
                                            //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());

                                        }
                                    }
        );

    }
}
