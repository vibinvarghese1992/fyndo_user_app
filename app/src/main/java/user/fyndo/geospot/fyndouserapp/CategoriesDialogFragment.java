package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 27/09/16.
 */

public class CategoriesDialogFragment extends DialogFragment {

    Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    StoreFragment storeFragment;
    EateryFragment eateryFragment;
    SharedPreferences prefs;

    @SuppressLint("ValidFragment")
    public CategoriesDialogFragment(Context context) {
        this.context = context;
    }

    public CategoriesDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.category_selection_dialog, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        TextView okButton = (TextView) rootView.findViewById(R.id.ok_button);
        TextView cancelButton = (TextView) rootView.findViewById(R.id.cancel_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFeedList();
            }
        });

        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        return rootView;
    }

    protected void updateFeedList() {
        String bizType;
        String bizArea;
        JSONArray bizCategories = new JSONArray();
        String pos;
        bizArea = ((MainActivity) getActivity()).shopArea.getSelectedItem().toString();

        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();

        TextView selectedCategories = ((MainActivity) getActivity()).selectedCategories;
        selectedCategories.setText("");
        String appendSelectedCategories = "";

        if (tabLayout.getSelectedTabPosition() == 1) {
            bizType = "Eatery";
            for (int i = 0; i < eateryFragment.categoryListAdapter.checkedPositions.size(); i++) {
                pos = eateryFragment.categoryListAdapter.checkedPositions.get(i);
                bizCategories.put(eateryFragment.categoryListAdapter.listItems.get(Integer.valueOf(pos)));
                appendSelectedCategories += eateryFragment.categoryListAdapter.listItems.get(Integer.parseInt(pos));
                if (i < eateryFragment.categoryListAdapter.checkedPositions.size() - 1)
                    appendSelectedCategories += ", ";
            }

            String json = gson.toJson(eateryFragment.categoryListAdapter.checkedPositions);
            editor.putString(Constants.SELECTED_EATERY_CATEGORIES, json);
            editor.apply();

        } else {
            bizType = "Store";
            for (int i = 0; i < storeFragment.categoryListAdapter.checkedPositions.size(); i++) {
                pos = storeFragment.categoryListAdapter.checkedPositions.get(i);
                bizCategories.put(storeFragment.categoryListAdapter.listItems.get(Integer.valueOf(pos)));
                appendSelectedCategories += storeFragment.categoryListAdapter.listItems.get(Integer.parseInt(pos));
                if (i < storeFragment.categoryListAdapter.checkedPositions.size() - 1)
                    appendSelectedCategories += ", ";
            }

            String json = gson.toJson(storeFragment.categoryListAdapter.checkedPositions);
            editor.putString(Constants.SELECTED_STORE_CATEGORIES, json);
            editor.apply();
        }

        editor.putString(Constants.SELECTED_CATEGORIES, appendSelectedCategories);
        editor.putString(Constants.SELECTED_BIZ_TYPE, bizType);
        editor.putString(Constants.SELECTED_CATEGORIES_ARRAY, bizCategories.toString());
        editor.apply();

        selectedCategories.setText(appendSelectedCategories);

        JSONObject postDataParams = new JSONObject();
        try {
            postDataParams.put("bizArea", bizArea);
            postDataParams.put("bizType", bizType);
            postDataParams.put("bizCategories", bizCategories);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "GetFeeds",
                "http://fyndox.herokuapp.com/api/selectFeeds", postDataParams, "POST");

        performAsyncTask.execute();*/

        ((MainActivity) getActivity()).recyclerView.setVisibility(View.GONE);
        ((MainActivity) getActivity()).progressBar.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).noFeeds.setVisibility(View.GONE);

        dismiss();

    }

    private void setupViewPager(ViewPager viewPager) {
        storeFragment = new StoreFragment(((MainActivity) context).shopCategoryStore);
        eateryFragment = new EateryFragment(((MainActivity) context).shopCategoryEatery);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(storeFragment, "Store");
        adapter.addFragment(eateryFragment, "Eatery");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
