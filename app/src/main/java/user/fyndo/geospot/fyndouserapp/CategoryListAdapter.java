package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 27/09/16.
 */

public class CategoryListAdapter extends BaseAdapter {

    ArrayList<String> listItems;
    Context context;
    LayoutInflater inflater;

    ArrayList<String> checkedPositions = new ArrayList<>();

    public CategoryListAdapter(Context context, ArrayList<String> listItems, ArrayList<String> checkedPositions) {
        this.listItems = listItems;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (checkedPositions != null)
            this.checkedPositions = checkedPositions;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView checkedTextView;
        public CheckBox checkBox;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;

        if (convertView == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.checkable_list_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.checkedTextView = (TextView) vi.findViewById(R.id.title);
            holder.checkBox = (CheckBox) vi.findViewById(R.id.check);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        holder.checkedTextView.setText(listItems.get(position));

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedPositions.contains(String.valueOf(position))) {
                    holder.checkBox.setChecked(false);
                    checkedPositions.remove(String.valueOf(position));
                } else {
                    holder.checkBox.setChecked(true);
                    checkedPositions.add(String.valueOf(position));
                }
            }
        });

        holder.checkedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.checkBox.isChecked()) {
                    checkedPositions.add(String.valueOf(position));
                    holder.checkBox.setChecked(true);
                } else {
                    checkedPositions.remove(String.valueOf(position));
                    holder.checkBox.setChecked(false);
                }
            }
        });

        if (checkedPositions.contains(String.valueOf(position))) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        return vi;
    }
}
