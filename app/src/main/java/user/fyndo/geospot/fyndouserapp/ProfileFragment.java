package user.fyndo.geospot.fyndouserapp;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by vibinvarghese on 24/10/16.
 */

public class ProfileFragment extends Fragment {

    ImageView profileImage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile_fragment, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        profileImage = (ImageView) rootView.findViewById(R.id.profile_image);

        TextView name = (TextView) rootView.findViewById(R.id.name);
        TextView email = (TextView) rootView.findViewById(R.id.email);

        name.setText("Name : " + prefs.getString(Constants.FB_USER_NAME, ""));
        email.setText("Email : " + prefs.getString(Constants.FB_USER_EMAIL, ""));

        ImageView cross = (ImageView) rootView.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) getActivity()).bottomTab.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("ProfileFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        String path = "https://graph.facebook.com/" + prefs.getString(Constants.FB_USER_ID, "") + "/picture?width=120&height=120" +
                "";

        Picasso.with(getActivity()).load(path)
                .resize(120, 120)
                .placeholder(R.drawable.ic_account)
                .centerCrop()
                .error(R.drawable.ic_account)
                .into(profileImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getWidth()) / 2.0f);
                        profileImage.setImageDrawable(imageDrawable);
                        profileImage.setPadding(8, 8, 8, 8);
                    }

                    @Override
                    public void onError() {
                        profileImage.setPadding(30, 30, 30, 30);
                    }
                });
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Picasso.with(getActivity()).cancelRequest(profileImage);
    }
}
