package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 23/11/16.
 */

public class BizFeedsFragment extends Fragment {

    int bizId;
    String shopArea, shopName;

    RecyclerView recyclerView;
    ProgressBar progressBar;

    ArrayList<FeedItemModel> feedArrayList = new ArrayList<>();
    FeedListRecyclerAdapter feedListRecyclerAdapter;

    SharedPreferences prefs;

    public BizFeedsFragment() {
    }

    @SuppressLint("ValidFragment")
    public BizFeedsFragment(int bizId, String shopArea, String shopName) {
        this.bizId = bizId;
        this.shopArea = shopArea;
        this.shopName = shopName;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.biz_feed_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        TextView shopNameTextView = (TextView) rootView.findViewById(R.id.shop_name);
        TextView shopAreaTextView = (TextView) rootView.findViewById(R.id.shop_area);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        ImageView maps = (ImageView) rootView.findViewById(R.id.maps);
        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).bizInfoFragment == null) {
                    ((MainActivity) getActivity()).launchBizDetailFragment(shopName, shopArea, bizId, 0, 0);
                }
            }
        });

        shopAreaTextView.setText(shopArea);
        shopNameTextView.setText(shopName);

        ImageView cross = (ImageView) rootView.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).bottomTab.setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });


        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("BizFeedsFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "GetBizFeeds",
                Constants.SELECT_FEEDS_BY_BIZ_ID + bizId, null, "GET");

        performAsyncTask.execute();


        return rootView;
    }

    protected void populateBizFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something Went wrong, please try again", recyclerView);
            return;
        }

        try {
            populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFeedsArray(String response) throws JSONException {

        final JSONArray feedsArray = new JSONArray(response);


        feedArrayList.clear();

        for (int i = 0; i < feedsArray.length(); i++) {

            JSONObject feed = feedsArray.getJSONObject(i);
            if (feed.isNull("linkedFiles"))
                continue;

            FeedItemModel feedItemModel = new FeedItemModel();
            feedItemModel.setTitle(feed.getString("title"));
            int id = feed.getInt("feedId");

            feedItemModel.setRating((int) feed.getDouble("userRating"));

            if (!feedItemModel.getTitle().contains("[")) {
                feedItemModel.setMainTitle(feedItemModel.getTitle());
            }

            if (!feed.isNull("bizInfo")) {
                feedItemModel.setShopImage(feed.getJSONObject("bizInfo").getString("bizProfile"));
            } else {
                feedItemModel.setShopImage("");
            }

            feedItemModel.setMainDescription(feed.getString("textContent"));

            feedItemModel.setLat(feed.getDouble("gpsLatitude"));
            feedItemModel.setLng(feed.getDouble("gpsLongitude"));
            feedItemModel.setMainCategory(feed.getString("sourceDevice"));

            feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
            feedItemModel.setId(id);
            feedItemModel.setDescription(feed.getString("sourceDevice"));
            feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
            feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
            String validity = "1";
            if (feed.getInt("numExpiryDays") > 0) {
                validity = String.valueOf(feed.getInt("numExpiryDays"));
            }
            feedItemModel.setValidity(validity);
            feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));
            feedItemModel.setDealType(feed.getString("feedType"));

            feedItemModel.setShopName("");
            feedItemModel.setShopArea("");
            feedItemModel.setBizId(feed.getInt("bizId"));

            if (!feed.isNull("shopName"))
                feedItemModel.setShopName(feed.getString("shopName"));

            if (!feed.isNull("shopArea"))
                feedItemModel.setShopArea(feed.getString("shopArea"));

            feedArrayList.add(feedItemModel);
        }

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        feedListRecyclerAdapter = new FeedListRecyclerAdapter(getActivity(), feedArrayList, prefs.getString(Constants.LIKED_FEED_IDS, ""), false);
        feedListRecyclerAdapter.notifyDataSetChanged();

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedListRecyclerAdapter);

    }
}
