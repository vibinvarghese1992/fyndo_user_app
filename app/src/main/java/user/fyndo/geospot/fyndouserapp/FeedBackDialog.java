package user.fyndo.geospot.fyndouserapp;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by vibinvarghese on 14/12/16.
 */

public class FeedBackDialog extends DialogFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.feedback_dialog, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        final Drawable d = new ColorDrawable(Color.TRANSPARENT);
        getDialog().getWindow().setBackgroundDrawable(d);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        final EditText feedback = (EditText) rootView.findViewById(R.id.feed_back);
        final Button send = (Button) rootView.findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedback.getText().length() == 0) {
                    Util.showSnackBar("Please enter some feedback", send);
                    return;
                }

                JSONObject postDataParams = new JSONObject();

                try {
                    postDataParams.put("userId", prefs.getString(Constants.USER_ID, "") + " + " + String.valueOf(System.currentTimeMillis()));
                    postDataParams.put("content", feedback.getText().toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "GetCustomFeeds",
                        Constants.INSERT_FEEDBACK, postDataParams, "POST");

                performAsyncTask.execute();

                Toast.makeText(getActivity(), "Thank you for your feedback", Toast.LENGTH_SHORT).show();

                AnalyticsApplication application = (AnalyticsApplication) getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("FeedBackSubmitted");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                dismiss();
            }
        });

        return rootView;
    }

}
