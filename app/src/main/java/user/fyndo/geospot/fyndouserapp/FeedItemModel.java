package user.fyndo.geospot.fyndouserapp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;

import java.util.Arrays;

/**
 * Created by vibinvarghese on 03/09/16.
 */

public class FeedItemModel implements Parcelable {
    String title, description, mainTitle, mainDescription;
    JSONArray imageURLs;
    String likes;
    String validity;
    String shopImage;
    String shopName;
    String mainCategory;
    int bizId;
    String dealType;
    long dateOfPost;
    int rating;
    JSONArray subCategories;

    int id;
    double lat, lng;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FeedItemModel(int rating, JSONArray subCategories, String mainTitle, String mainDescription, String dealType, long dateOfPost, int bizId, String mainCategory, double lat, double lng, String title, String description, JSONArray imageURLs, String likes,
                         String validity, String shopArea, String shopImage, String shopName, int id) {
        this.dateOfPost = dateOfPost;
        this.title = title;
        this.description = description;
        this.imageURLs = imageURLs;
        this.likes = likes;
        this.validity = validity;
        this.shopArea = shopArea;
        this.shopImage = shopImage;
        this.shopName = shopName;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.mainCategory = mainCategory;
        this.bizId = bizId;
        this.dealType = dealType;
        this.mainTitle = mainTitle;
        this.mainDescription = mainDescription;
        this.subCategories = subCategories;
        this.rating = rating;

    }

    public JSONArray getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(JSONArray subCategories) {
        this.subCategories = subCategories;
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getMainDescription() {
        return mainDescription;
    }

    public void setMainDescription(String mainDescription) {
        this.mainDescription = mainDescription;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public long getDateOfPost() {
        return dateOfPost;
    }

    public void setDateOfPost(long dateOfPost) {
        this.dateOfPost = dateOfPost;
    }

    public int getBizId() {
        return bizId;
    }

    public void setBizId(int bizId) {
        this.bizId = bizId;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {

        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    protected FeedItemModel(Parcel in) {
        title = in.readString();
        description = in.readString();
        likes = in.readString();
        validity = in.readString();
        shopImage = in.readString();
        shopName = in.readString();
        shopArea = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        bizId = in.readInt();
        rating = in.readInt();
    }

    public static final Creator<FeedItemModel> CREATOR = new Creator<FeedItemModel>() {
        @Override
        public FeedItemModel createFromParcel(Parcel in) {
            return new FeedItemModel(in);
        }

        @Override
        public FeedItemModel[] newArray(int size) {
            return new FeedItemModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(shopArea);
        dest.writeString(shopName);
        dest.writeString(shopImage);
        dest.writeString(imageURLs.toString());
        dest.writeInt(rating);
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getShopArea() {
        return shopArea;
    }

    public void setShopArea(String shopArea) {
        this.shopArea = shopArea;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    String shopArea;

    public FeedItemModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JSONArray getImageURLs() {
        return imageURLs;
    }

    public void setImageURLs(JSONArray imageURLs) {
        this.imageURLs = imageURLs;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
