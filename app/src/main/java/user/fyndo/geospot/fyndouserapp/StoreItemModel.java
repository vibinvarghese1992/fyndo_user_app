package user.fyndo.geospot.fyndouserapp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;

/**
 * Created by vibinvarghese on 03/09/16.
 */

public class StoreItemModel implements Parcelable {

    String shopName, shopArea, shopPhone, shopImage;
    int bizId;
    double lat, lng;
    String bizCategories;

    public StoreItemModel(String shopImage, String shopArea, String shopName, String shopPhone, int bizId, double lat, double lng, String bizCategories) {
        this.shopArea = shopArea;
        this.shopImage = shopImage;
        this.shopName = shopName;
        this.shopPhone = shopPhone;
        this.bizId = bizId;
        this.lat = lat;
        this.lng = lng;
        this.bizCategories = bizCategories;
    }

    public StoreItemModel() {
    }

    protected StoreItemModel(Parcel in) {

    }

    public static final Creator<StoreItemModel> CREATOR = new Creator<StoreItemModel>() {
        @Override
        public StoreItemModel createFromParcel(Parcel in) {
            return new StoreItemModel(in);
        }

        @Override
        public StoreItemModel[] newArray(int size) {
            return new StoreItemModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shopPhone);
        dest.writeString(bizCategories);
        dest.writeString(shopArea);
        dest.writeString(shopName);
        dest.writeString(shopImage);
        dest.writeInt(bizId);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopArea() {
        return shopArea;
    }

    public void setShopArea(String shopArea) {
        this.shopArea = shopArea;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public int getBizId() {
        return bizId;
    }

    public void setBizId(int bizId) {
        this.bizId = bizId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getBizCategories() {
        return bizCategories;
    }

    public void setBizCategories(String bizCategories) {
        this.bizCategories = bizCategories;
    }

    public static Creator<StoreItemModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
