package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 09/01/17.
 */

public class SubCategorySelectionFragment extends Fragment {


    CardView header;

    ArrayList<String> selectedMen = new ArrayList<>();
    ArrayList<String> selectedWomen = new ArrayList<>();
    ArrayList<String> selectedConsumerElectronics = new ArrayList<>();
    ArrayList<String> selectedHomeAppliances = new ArrayList<>();
    ArrayList<String> selectedHomeFurnitures = new ArrayList<>();
    ArrayList<String> selectedBabyNKids = new ArrayList<>();

    SharedPreferences prefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.sub_category_picker_scroll_fragment, container, false);

        header = (CardView) rootView.findViewById(R.id.header);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.men_main), R.id.men, ((MainActivity) getActivity()).men, Constants.USER_PREFERENCES_MEN);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.women_main), R.id.women, ((MainActivity) getActivity()).women, Constants.USER_PREFERENCES_WOMEN);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.kids_main), R.id.kids, ((MainActivity) getActivity()).babyNKids, Constants.USER_PREFERENCES_BABY_N_KIDS);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.electronics_main), R.id.electronics, ((MainActivity) getActivity()).consumerElectronics, Constants.USER_PREFERENCES_CONSUMER_ELECTRONICS);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.appliance_main), R.id.appliance, ((MainActivity) getActivity()).homeAppliances, Constants.USER_PREFERENCES_HOME_APPLIANCES);
        insertPreferences((FlexboxLayout) rootView.findViewById(R.id.furniture_main), R.id.furniture, ((MainActivity) getActivity()).homeFurnitures, Constants.USER_PREFERENCES_HOME_FURNITURE);

        ImageView tick = (ImageView) rootView.findViewById(R.id.tick);
        tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveUserPreferences();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        CheckBox selectAllMen = (CheckBox) rootView.findViewById(R.id.select_all_men);
        CheckBox selectAllWomen = (CheckBox) rootView.findViewById(R.id.select_all_women);
        CheckBox selectAllKids = (CheckBox) rootView.findViewById(R.id.select_all_kids);
        CheckBox selectAllElectronics = (CheckBox) rootView.findViewById(R.id.select_all_electronics);
        CheckBox selectAllAppliances = (CheckBox) rootView.findViewById(R.id.select_all_appliances);
        CheckBox selectAllFurniture = (CheckBox) rootView.findViewById(R.id.select_all_furniture);

        selectAllMen.setOnCheckedChangeListener(checkedChangeListener);
        selectAllWomen.setOnCheckedChangeListener(checkedChangeListener);
        selectAllKids.setOnCheckedChangeListener(checkedChangeListener);
        selectAllElectronics.setOnCheckedChangeListener(checkedChangeListener);
        selectAllAppliances.setOnCheckedChangeListener(checkedChangeListener);
        selectAllFurniture.setOnCheckedChangeListener(checkedChangeListener);


        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("PreferenceSelectionFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        return rootView;
    }

    CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            processSelectAll(buttonView, isChecked);
        }
    };

    @SuppressLint("WrongViewCast")
    protected void processSelectAll(CompoundButton button, boolean isChecked) {
        FlexboxLayout parentView;
        switch (button.getId()) {
            case R.id.select_all_men:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.men_main);
                processSelectAllElements(selectedMen, isChecked, parentView);
                break;
            case R.id.select_all_women:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.women_main);
                processSelectAllElements(selectedWomen, isChecked, parentView);
                break;
            case R.id.select_all_kids:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.kids_main);
                processSelectAllElements(selectedBabyNKids, isChecked, parentView);
                break;
            case R.id.select_all_electronics:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.electronics_main);
                processSelectAllElements(selectedConsumerElectronics, isChecked, parentView);
                break;
            case R.id.select_all_appliances:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.appliance_main);
                processSelectAllElements(selectedHomeAppliances, isChecked, parentView);
                break;
            case R.id.select_all_furniture:
                parentView = (FlexboxLayout) ((LinearLayout) button.getParent().getParent()).findViewById(R.id.furniture_main);
                processSelectAllElements(selectedHomeFurnitures, isChecked, parentView);
                break;
        }
    }

    protected void processSelectAllElements(ArrayList<String> checkList, boolean isChecked, FlexboxLayout parentView) {
        if (isChecked) {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (!checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        } else {
            for (int i = 0; i < parentView.getChildCount(); i++) {
                String itemText = (String) ((TextView) ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1)).getText();
                if (checkList.contains(itemText)) {
                    ((LinearLayout) parentView.getChildAt(i)).findViewById(R.id.item1).callOnClick();
                }
            }
        }
    }

    protected void saveUserPreferences() throws JSONException {

        if ((selectedMen.size() + selectedWomen.size()) < 5) {
            Util.showSnackBar("Please select at least 5", header);
            return;
        }


        ((MainActivity) getActivity()).currentFeedCount = 20;
        ((MainActivity) getActivity()).dynamicFeed = 0;
        ((MainActivity) getActivity()).feedArrayList.clear();
        ((MainActivity) getActivity()).feedListRecyclerAdapter.notifyDataSetChanged();

        JSONArray sample = new JSONArray();
        //---Setting Preferences ------

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());


        JSONArray filterCategories = new JSONArray();
        JSONObject subCategoryObject;


        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(selectedMen);
        editor.putString(Constants.USER_PREFERENCES_MEN, json);
        if (selectedMen.size() > 0) {
            sample.put("Men");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Men");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedWomen);
        editor.putString(Constants.USER_PREFERENCES_WOMEN, json);
        if (selectedWomen.size() > 0) {
            sample.put("Women");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Women");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedConsumerElectronics);
        editor.putString(Constants.USER_PREFERENCES_CONSUMER_ELECTRONICS, json);
        if (selectedConsumerElectronics.size() > 0) {
            sample.put("Consumer Electronics");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Consumer Electronics");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedHomeAppliances);
        editor.putString(Constants.USER_PREFERENCES_HOME_APPLIANCES, json);
        if (selectedHomeAppliances.size() > 0) {
            sample.put("Home Appliances");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Home Appliances");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedHomeFurnitures);
        editor.putString(Constants.USER_PREFERENCES_HOME_FURNITURE, json);
        if (selectedHomeFurnitures.size() > 0) {
            sample.put("Home Furnitures");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Home Furnitures");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }
        json = gson.toJson(selectedBabyNKids);
        editor.putString(Constants.USER_PREFERENCES_BABY_N_KIDS, json);
        if (selectedBabyNKids.size() > 0) {
            sample.put("Baby & Kids");
            subCategoryObject = new JSONObject();
            subCategoryObject.put("userCatLevel1", "Baby & Kids");
            subCategoryObject.put("userCatLevel2", new JSONArray(json));
            filterCategories.put(subCategoryObject);
        }

        editor.putBoolean(Constants.USER_PREFERENCES_ISSET, true);

        editor.apply();

        //-----------------------------


        ((MainActivity) getActivity()).currentSelected = sample;

        ((MainActivity) getActivity()).makeCustomRequestToPopulateAllFeeds(true, true);

        ((MainActivity) getActivity()).setSelectedCategoriesText();

        // ---- update user  object --------

        JSONObject postDataParams = new JSONObject();

        try {
            postDataParams.put("preferredCategories", filterCategories);
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "UpdateUserPrefs",
                Constants.UPDATE_USER, postDataParams, "POST");

        performAsyncTask.execute();


        // ---------------------------------


        getActivity().onBackPressed();
    }

    protected void insertPreferences(FlexboxLayout parentView, int id, ArrayList<String> preferences, String savedKey) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String json;
        json = prefs.getString(savedKey, null);
        JSONArray jsonArray = new JSONArray();
        ArrayList<String> selectedItems = new ArrayList<>();
        Gson gson = new Gson();
        if (json != null) {
            try {
                jsonArray = new JSONArray(json);
                Type type = new TypeToken<List<String>>() {
                }.getType();
                selectedItems = gson.fromJson(json, type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < preferences.size(); i++) {

            String category = preferences.get(i);
            View view = (View) inflater.inflate(R.layout.single_item_textview, null);

            if (json != null) {
                if (selectedItems.contains(category)) {
                    ((TextView) view.findViewById(R.id.item1)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
                    ((TextView) view.findViewById(R.id.item1)).setTextColor(getActivity().getResources().getColor(android.R.color.black));

                    if (id == R.id.men)
                        selectedMen.add(category);
                    if (id == R.id.women)
                        selectedWomen.add(category);
                    if (id == R.id.kids)
                        selectedBabyNKids.add(category);
                    if (id == R.id.electronics)
                        selectedConsumerElectronics.add(category);
                    if (id == R.id.appliance)
                        selectedHomeAppliances.add(category);
                    if (id == R.id.furniture)
                        selectedHomeFurnitures.add(category);
                }
            }
            TextView itemTextView1 = (TextView) view.findViewById(R.id.item1);
            itemTextView1.setOnClickListener(onClickListener);
            itemTextView1.setText(category);
            parentView.addView(view);
        }
    }

    protected ArrayList<String> processSelectedItem(ArrayList<String> selectedPrefs, String choice, View v) {
        if (selectedPrefs.indexOf(choice) > -1) {
            selectedPrefs.remove(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.unselected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        } else {
            selectedPrefs.add(choice);
            ((TextView) v).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selected_border_preference_tabs));
            ((TextView) v).setTextColor(getActivity().getResources().getColor(android.R.color.black));
        }
        return selectedPrefs;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int selctedId = ((FlexboxLayout) v.getParent().getParent()).getId();

            switch (selctedId) {
                case R.id.men_main:
                    selectedMen = processSelectedItem(selectedMen, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.women_main:
                    selectedWomen = processSelectedItem(selectedWomen, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.kids_main:
                    selectedBabyNKids = processSelectedItem(selectedBabyNKids, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.electronics_main:
                    selectedConsumerElectronics = processSelectedItem(selectedConsumerElectronics, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.appliance_main:
                    selectedHomeAppliances = processSelectedItem(selectedHomeAppliances, ((TextView) v).getText().toString(), v);
                    break;
                case R.id.furniture_main:
                    selectedHomeFurnitures = processSelectedItem(selectedHomeFurnitures, ((TextView) v).getText().toString(), v);
                    break;
            }
        }
    };
}
