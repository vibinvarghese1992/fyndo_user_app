package user.fyndo.geospot.fyndouserapp;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 08/11/16.
 */

public class FeedDetailFragment extends Fragment {

    FeedItemModel feedItemModel;
    ViewPagerAdapterForDialogFragment mAdapter;
    LinearLayout pager_indicator;
    int dotsCount;
    ImageView[] dots;
    ImageView mHeart;
    TextView mLikes;
    Context mContext;
    boolean liked = false;
    int position;

    public FeedDetailFragment() {
    }

    @SuppressLint("ValidFragment")
    public FeedDetailFragment(FeedItemModel feedItemModel, ImageView heart, TextView likes, int position) {
        this.feedItemModel = feedItemModel;
        this.mHeart = heart;
        this.mLikes = likes;
        this.position = position;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_detail_fragment, container, false);

        mContext = getContext();

        TextView title = (TextView) rootView.findViewById(R.id.title);
        TextView time = (TextView) rootView.findViewById(R.id.time);
        TextView shopArea = (TextView) rootView.findViewById(R.id.shop_area);
        TextView shopName = (TextView) rootView.findViewById(R.id.shop_name);
        final CustomViewPager viewPager = (CustomViewPager) rootView.findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) rootView.findViewById(R.id.viewPagerCountDots);
        ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        //title.setText(feedItemModel.getTitle().replace("[", "").replace("]", ""));

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("FeedDetailFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        time.setText((String) DateUtils.getRelativeTimeSpanString(feedItemModel.getDateOfPost(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        String[] areaArray = feedItemModel.getShopArea().split(", ");
        String extractedShopArea = feedItemModel.getShopArea();
        for (int k = 0; k < areaArray.length; k++) {
            if (areaArray[k].equals("Chennai")) {
                extractedShopArea = areaArray[k - 1];
                break;
            }
        }


        shopArea.setText(extractedShopArea);
        shopName.setText(feedItemModel.getShopName());
        ImageView shopIcon = (ImageView) rootView.findViewById(R.id.shop_icon);

        int shopIconInt = ((MainActivity) mContext).feedListRecyclerAdapter.getShopImage(feedItemModel.getMainCategory());

        //shopIcon.setImageDrawable(mContext.getResources().getDrawable(shopIconInt));

        TextView distanceTextView = (TextView) rootView.findViewById(R.id.distance);

        double distance = Util.distance(getActivity(), feedItemModel.getLat(), feedItemModel.getLng());

        distance = distance / 1000;

        if (distance > 100) {
            distanceTextView.setText(" ~5" + " km away");
        } else {
            distanceTextView.setText(String.format("%.2f", distance) + " km away");
        }


        LinearLayout likeLayout = (LinearLayout) rootView.findViewById(R.id.like_layout);
        final ImageView heart = (ImageView) rootView.findViewById(R.id.heart_image);
        final TextView likes = (TextView) rootView.findViewById(R.id.likes);

        likes.setText(String.valueOf(feedItemModel.getLikes()));

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        final String likedFeeds = prefs.getString(Constants.LIKED_FEED_IDS, "");

        ImageView cross = (ImageView) rootView.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) getActivity()).fab.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).bottomTab.setVisibility(View.VISIBLE);
                //getActivity().getSupportFragmentManager().beginTransaction().remove(((MainActivity) getActivity()).feedDetailFragment).commit();
                //getActivity().getSupportFragmentManager().popBackStack();
                getActivity().onBackPressed();
            }
        });

        ImageView call = (ImageView) rootView.findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+917349712843"));
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Util.showSnackBar("Please grant permission to make a call", ((MainActivity) getActivity()).recyclerView);
                    return;
                }

                AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("CallTriggered");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                getActivity().startActivity(intent);
            }
        });

        likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (likedFeeds.contains(String.valueOf(feedItemModel.getId()))) {
                    Util.showSnackBar("Feed Already Liked", heart);
                    return;
                }

                if (liked) {
                    Util.showSnackBar("Feed Already Liked", heart);
                    return;
                }

                liked = true;

                if (position > -1)
                    ((MainActivity) getActivity()).upVoteFeed(feedItemModel.getId(), position);

                heart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.heart_filled));
                if (position > -1)
                    mHeart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.heart_filled));
                int like = Integer.parseInt(likes.getText().toString());
                likes.setText(String.valueOf(++like));
                if (position > -1)
                    mLikes.setText(String.valueOf(like));

                JSONObject postDataParams = new JSONObject();
                Location location = Util.getLocation(getActivity());
                try {
                    postDataParams.put("feedId", feedItemModel.getId());
                    postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));
                    postDataParams.put("gpsLatitude", location.getLatitude());
                    postDataParams.put("gpsLongitude", location.getLongitude());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "UpVote",
                        Constants.UPVOTE_FEED, postDataParams, "POST");

                performAsyncTask.execute();
            }
        });

        if (likedFeeds.contains(String.valueOf(feedItemModel.getId()))) {
            heart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.heart_filled));
        } else {
            heart.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.heart));
        }

        String imageURL1 = "", imageURL2 = "", imageURL3 = "";
        try {
            imageURL1 = feedItemModel.getImageURLs().get(0).toString();
            imageURL2 = feedItemModel.getImageURLs().get(1).toString();
            imageURL3 = feedItemModel.getImageURLs().get(2).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray images = new JSONArray();

        if (imageURL1.length() != 0 && !imageURL1.equals("") && !imageURL1.equals("null")) {
            images.put(imageURL1);
            //images.put("http://lorempixel.com/300/300/cats/1/");
        }
        if (imageURL2.length() != 0 && !imageURL2.equals("") && !imageURL2.equals("null")) {
            images.put(imageURL2);
            //images.put("http://lorempixel.com/300/300/cats/1/");
        }
        if (imageURL3.length() != 0 && !imageURL3.equals("") && !imageURL3.equals("null")) {
            images.put(imageURL3);
            //images.put("http://lorempixel.com/300/300/cats/1/");
        }

        if (images.length() < 2)
            pager_indicator.setVisibility(View.GONE);

        mAdapter = new ViewPagerAdapterForDialogFragment(getActivity(), images, progressBar, pager_indicator.getWidth(), pager_indicator.getHeight());
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];
        setUiPageViewController();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < dotsCount; i++) {
                    //dots[i] = new ImageView(getActivity());
                    dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.nonselecteditem_dot_white));
                }
                dots[position].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state != ViewPager.SCROLL_STATE_IDLE) {
                    final int childCount = viewPager.getChildCount();
                    for (int i = 0; i < childCount; i++)
                        viewPager.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }
        });


        return rootView;
    }

    private void setUiPageViewController() {

        pager_indicator.removeAllViews();

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.nonselecteditem_dot_white));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }
        if (dots.length != 0)
            dots[0].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.selecteditem_dot));
    }
}


class ViewPagerAdapterForDialogFragment extends PagerAdapter {

    private Context mContext;
    private JSONArray mResources;
    ProgressBar progressBar;
    int width, height;

    public ViewPagerAdapterForDialogFragment(Context mContext, JSONArray mResources, ProgressBar progressBar, int width, int height) {
        this.mContext = mContext;
        this.mResources = mResources;
        this.progressBar = progressBar;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mResources.length();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item_feed_detail, container, false);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);

        progressBar.setVisibility(View.VISIBLE);

        String imageURL = null;

        try {
            imageURL = mResources.getString(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (imageURL == null)
            return itemView;

        //Log.d("image", Constants.ROOT_URL + imageURL);

        Picasso.with(mContext)
                .load(Constants.ROOT_URL + imageURL)
                .error(R.drawable.image_place_holder)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.image_place_holder));
                    }
                });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
