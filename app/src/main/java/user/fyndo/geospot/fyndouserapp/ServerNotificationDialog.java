package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vibinvarghese on 14/12/16.
 */

public class ServerNotificationDialog extends DialogFragment {

    String response;

    public ServerNotificationDialog() {
    }

    @SuppressLint("ValidFragment")
    public ServerNotificationDialog(String response) {
        this.response = response;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.server_notification_dialog, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        final Drawable d = new ColorDrawable(Color.TRANSPARENT);
        getDialog().getWindow().setBackgroundDrawable(d);

        JSONArray responseArray;
        JSONObject responseObject = new JSONObject();
        String title = "", description = "", imageURL = "", callToAction = "";

        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("ServerPopUpTriggered");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        String appVersionString = "";
        try {
            responseArray = new JSONArray(response);
            responseObject = responseArray.getJSONObject(0);
            title = responseObject.getString("title");
            description = responseObject.getString("description");
            imageURL = responseObject.getString("imageUrl");
            callToAction = responseObject.getString("callToActionUrl");
            appVersionString = responseObject.getString("targetAppVersion");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView titleView = (TextView) rootView.findViewById(R.id.title);
        TextView descriptionView = (TextView) rootView.findViewById(R.id.description);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.image);
        ImageView close = (ImageView) rootView.findViewById(R.id.close);
        Button button = (Button) rootView.findViewById(R.id.action_button);

        String buttonString = appVersionString.split("\\+")[1];
        button.setText(buttonString);

        String flagShowCross = appVersionString.split("\\+")[2];

        if (flagShowCross.equals("0")) {
            close.setVisibility(View.INVISIBLE);
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("ServerPopUpCloseClicked");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                dismiss();
            }
        });

        titleView.setText(title);
        descriptionView.setText(description);

        if (!imageURL.equals(""))
            Picasso.with(getActivity())
                    .load(imageURL)
                    .centerCrop()
                    .resize(400, 400)
                    .placeholder(R.drawable.image_place_holder)
                    .error(R.drawable.image_place_holder)
                    .into(imageView);

        final String finalCallToAction = callToAction;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
                Tracker mTracker = application.getDefaultTracker();
                mTracker.setScreenName("ServerPopUpCallToActionClicked");
                mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                if (finalCallToAction.equals("")) {
                    dismiss();
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(finalCallToAction));
                    startActivity(i);
                    dismiss();
                }
            }
        });


        return rootView;
    }
}
