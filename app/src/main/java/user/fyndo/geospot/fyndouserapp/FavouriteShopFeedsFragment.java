package user.fyndo.geospot.fyndouserapp;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 23/11/16.
 */

public class FavouriteShopFeedsFragment extends Fragment {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView noFavText;

    ArrayList<FeedItemModel> feedArrayList = new ArrayList<>();
    FeedListRecyclerAdapter feedListRecyclerAdapter;

    SharedPreferences prefs;

    public FavouriteShopFeedsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.favourite_shop_feed_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("FavouriteShopFeedsFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        feedListRecyclerAdapter = new FeedListRecyclerAdapter(getActivity(), feedArrayList, prefs.getString(Constants.LIKED_FEED_IDS, ""), false);
        feedListRecyclerAdapter.notifyDataSetChanged();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedListRecyclerAdapter);
        noFavText = (TextView) rootView.findViewById(R.id.no_fav_text);


        makeRequest();

        return rootView;
    }

    protected void makeRequest() {
        JSONObject postDataParams = new JSONObject();

        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        feedArrayList.clear();
        feedListRecyclerAdapter.notifyDataSetChanged();
        noFavText.setVisibility(View.GONE);

        Location location = Util.getLocation(getActivity());

        try {
            postDataParams.put("gpsLatitude", location.getLatitude());
            postDataParams.put("gpsLongitude", location.getLongitude());
            postDataParams.put("userId", prefs.getString(Constants.USER_ID, ""));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "FeedsOfFavouriteShop",
                Constants.SELECT_FEEDS_BY_FAVOURITE_BIZ, postDataParams, "POST");

        performAsyncTask.execute();
    }

    protected void populateBizFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something Went wrong, please try again", recyclerView);
            return;
        }

        try {
            populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFeedsArray(String response) throws JSONException {
        final JSONArray feedsArray = new JSONArray(response);


        if (feedArrayList.size() == 0) {
            progressBar.setVisibility(View.GONE);
            noFavText.setVisibility(View.VISIBLE);
        }

        //feedArrayList.clear();

        for (int i = 0; i < feedsArray.length(); i++) {

            JSONObject feed = feedsArray.getJSONObject(i);
            if (feed.isNull("linkedFiles"))
                continue;

            FeedItemModel feedItemModel = new FeedItemModel();
            feedItemModel.setTitle(feed.getString("title"));
            int id = feed.getInt("feedId");

            feedItemModel.setRating((int) feed.getDouble("userRating"));

            if (!feedItemModel.getTitle().contains("[")) {
                feedItemModel.setMainTitle(feedItemModel.getTitle());
            }

            if (!feed.isNull("bizInfo")) {
                feedItemModel.setShopImage(feed.getJSONObject("bizInfo").getString("bizProfile"));
            } else {
                feedItemModel.setShopImage("");
            }

            feedItemModel.setMainDescription(feed.getString("textContent"));

            feedItemModel.setLat(feed.getDouble("gpsLatitude"));
            feedItemModel.setLng(feed.getDouble("gpsLongitude"));
            feedItemModel.setMainCategory(feed.getString("sourceDevice"));

            feedItemModel.setId(id);
            feedItemModel.setDateOfPost(feed.getLong("dateOfPost"));
            feedItemModel.setDescription(feed.getString("sourceDevice"));
            feedItemModel.setImageURLs(feed.getJSONArray("linkedFiles"));
            feedItemModel.setSubCategories(feed.getJSONArray("userCatLevel2"));
            feedItemModel.setDealType(feed.getString("feedType"));
            String validity = "1";
            if (feed.getInt("numExpiryDays") > 0) {
                validity = String.valueOf(feed.getInt("numExpiryDays"));
            }
            feedItemModel.setValidity(validity);

            feedItemModel.setLikes(String.valueOf(feed.getInt("noOfUpvotes")));

            feedItemModel.setShopName("");
            feedItemModel.setShopArea("");
            feedItemModel.setBizId(feed.getInt("bizId"));

            if (!feed.isNull("shopName"))
                feedItemModel.setShopName(feed.getString("shopName"));

            if (!feed.isNull("shopArea"))
                feedItemModel.setShopArea(feed.getString("shopArea"));

            feedArrayList.add(feedItemModel);
        }

        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                progressBar.setVisibility(View.GONE);
                                                recyclerView.setVisibility(View.VISIBLE);
                                                feedListRecyclerAdapter.notifyDataSetChanged();
                                                //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());

                                            }
                                        }
            );

    }
}
