package user.fyndo.geospot.fyndouserapp;

import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 12/12/16.
 */

public class CategorySelectorFragment extends Fragment {

    ArrayList<Integer> categoryIcons = new ArrayList<>();
    ArrayList<String> selectedCategories = new ArrayList<>();

    JSONArray currentSelectedCategories;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.category_pick_fragment, container, false);

        final Button discover = (Button) rootView.findViewById(R.id.discovering);

        categoryIcons.add(R.id.men);
        categoryIcons.add(R.id.women);
        categoryIcons.add(R.id.kids);
        categoryIcons.add(R.id.furniture);
        categoryIcons.add(R.id.appliance);
        categoryIcons.add(R.id.electronics);

        CheckBox selectAll = (CheckBox) rootView.findViewById(R.id.select_all);

        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < categoryIcons.size(); i++) {
                        if (!selectedCategories.contains(String.valueOf(categoryIcons.get(i)))) {
                            ((LinearLayout) rootView.findViewById(categoryIcons.get(i))).callOnClick();
                        }
                    }
                } else {
                    for (int i = 0; i < categoryIcons.size(); i++) {
                        if (selectedCategories.contains(String.valueOf(categoryIcons.get(i)))) {
                            ((LinearLayout) rootView.findViewById(categoryIcons.get(i))).callOnClick();
                        }
                    }
                }
            }
        });

        discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSelectedCategories = new JSONArray();

                if (selectedCategories.size() == 0) {
                    Util.showSnackBar("Please pick atleast one category", discover);
                    return;
                }

                for (int k = 0; k < selectedCategories.size(); k++) {
                    currentSelectedCategories.put(((MainActivity) getActivity()).titleArray[categoryIcons.indexOf(Integer.parseInt(selectedCategories.get(k)))]);
                }

                ((MainActivity) getActivity()).currentFeedCount = 20;
                ((MainActivity) getActivity()).dynamicFeed = 0;
                ((MainActivity) getActivity()).feedArrayList.clear();
                ((MainActivity) getActivity()).feedListRecyclerAdapter.notifyDataSetChanged();

                ((MainActivity) getActivity()).currentSelected = currentSelectedCategories;

                ((MainActivity) getActivity()).makeCustomRequestToPopulateAllFeeds(true, false);

                ((MainActivity) getActivity()).setSelectedCategoriesText();

                getActivity().onBackPressed();

            }
        });


        for (int i = 0; i < categoryIcons.size(); i++) {
            rootView.findViewById(categoryIcons.get(i)).setOnClickListener(categoryItemClickListener);
        }

        return rootView;
    }

    View.OnClickListener categoryItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (selectedCategories.contains(String.valueOf(view.getId()))) {
                selectedCategories.remove(String.valueOf(view.getId()));
                for (int index = 0; index < ((ViewGroup) view).getChildCount(); ++index) {
                    View child = ((ViewGroup) view).getChildAt(index);
                    if (child instanceof ImageView) {
                        ((ImageView) child).setColorFilter(ContextCompat.getColor(getActivity(), R.color.cancelTextColor));
                    }
                    if (child instanceof TextView) {
                        ((TextView) child).setTextColor(getResources().getColor(R.color.cancelTextColor));
                    }
                }
            } else {
                selectedCategories.add(String.valueOf(view.getId()));
                for (int index = 0; index < ((ViewGroup) view).getChildCount(); ++index) {
                    View child = ((ViewGroup) view).getChildAt(index);
                    if (child instanceof ImageView) {
                        ((ImageView) child).setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                    }
                    if (child instanceof TextView) {
                        ((TextView) child).setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                }
            }
        }
    };
}
