package user.fyndo.geospot.fyndouserapp;

/**
 * Created by vibinvarghese on 27/09/16.
 */

public class Constants {
    public static final String ROOT_URL = "https://fyndox.herokuapp.com";
    public static final String BASE_URL = "https://fyndox.herokuapp.com/api/";

    public static final String SELECT_BIZ_AREAS = BASE_URL + "selectBizAreas/";
    public static final String SELECT_BIZ_CATEGORIES = BASE_URL + "selectBizCategories/";
    public static final String SELECT_DYNAMIC_FEEDS = BASE_URL + "selectDynamicFeeds2/";
    public static final String POPUP_LINK = BASE_URL + "selectActiveNotifications/";
    public static final String UPVOTE_FEED = BASE_URL + "upvoteFeed/";
    public static final String SELECT_BIZ_INFO = BASE_URL + "selectBizInfo/";
    public static final String SELECT_FEEDS_BY_BIZ_ID = BASE_URL + "selectFeedsByBizId/";
    public static final String SELECT_BIZ_INFOS_BY_CATEGORY = BASE_URL + "selectBizInfosByCategory/";
    public static final String SELECT_USER_BY_FB = BASE_URL + "selectUserByFbProfile/";
    public static final String INSERT_USER = BASE_URL + "insertUser/";
    public static final String UPDATE_USER = BASE_URL + "updateUser/";
    public static final String RATE_FEED = BASE_URL + "insertFeedReview/";
    public static final String INSERT_FAVOURITE = BASE_URL + "insertFavouriteFeed/";
    public static final String INSERT_FAVOURITE_BIZ_INFO = BASE_URL + "insertFavouriteBizInfo/";
    public static final String DELETE_FAVOURITE_BIZ_INFO = BASE_URL + "deleteFavouriteBizInfo/";
    public static final String DELETE_FAVOURITE = BASE_URL + "deleteFavouriteFeed/";
    public static final String SELECT_FEEDS_BY_FAVOURITE_BIZ = BASE_URL + "selectFeedsByFavouriteBiz/";
    public static final String SELECT_FAVOURITE_STORES = BASE_URL + "selectFavouriteBizInfos/";
    public static final String SELECT_FAVOURITE_FEEDS = BASE_URL + "selectFavouriteFeeds/";
    public static final String SELECT_FEED = BASE_URL + "selectFeed/";
    public static final String OPEN_FEED = BASE_URL + "openFeed/";
    public static final String INCREMENT_BIZ_CLICK = BASE_URL + "incrementBizClick/";
    public static final String INSERT_FEEDBACK = BASE_URL + "insertFeedback/";
    public static final String DEEPLINK_PREFIX = "https://z79tw.app.goo.gl/?link=https://www.fyndo.co?feedId=";
    public static final String DEEPLINK_SUFFIX = "&apn=user.fyndo.geospot.fyndouserapp";

    public static final String BIZ_AREA_LIST = "biz_area_list";
    public static final String LIKED_FEED_IDS = "liked_feed_ids";
    public static final String SHOP_CATEGORY_STORE = "shop_category_store";
    public static final String SHOP_CATEGORY_EATERY = "shop_category_eatery";
    public static final String SELECTED_STORE_CATEGORIES = "selected_store_categories";
    public static final String SELECTED_EATERY_CATEGORIES = "selected_eatery_categories";
    public static final String SELECTED_CATEGORIES = "selected_categories";
    public static final String SELECTED_AREA = "selected_area";
    public static final String SELECTED_BIZ_TYPE = "selected_biz_type";
    public static final String SELECTED_CATEGORIES_ARRAY = "selected_categories_array";
    public static final String SELECTED_MAIN_CATEGORIES_ARRAY = "selected_main_categories_array";

    public static final String FB_USER_ID = "fb_user_id";
    public static final String USER_ID = "user_id";
    public static final String FB_USER_NAME = "fb_user_name";
    public static final String FB_USER_EMAIL = "fb_user_email";
    public static final String FB_DOB = "dob";
    public static final String FB_GENDER = "gender";
    public static final String IS_LOGGED_IN = "is_logged_in_new";
    public static final String FIREBASE_API_KEY = "AIzaSyAw89x3agO6L3q66L2Pqz6yMnWIM7C7hS8";

    public static final String USER_PREFERENCES_MEN = "user_preferences_men";
    public static final String USER_PREFERENCES_WOMEN = "user_preferences_women";
    public static final String USER_PREFERENCES_CONSUMER_ELECTRONICS = "user_preferences_consumer_electronics";
    public static final String USER_PREFERENCES_HOME_APPLIANCES = "user_preferences_home_appliances";
    public static final String USER_PREFERENCES_HOME_FURNITURE = "user_preferences_home_furniture";
    public static final String USER_PREFERENCES_BABY_N_KIDS = "user_preferences_baby_n_kids";
    public static final String USER_PREFERENCES_ISSET = "user_preferences_isset";
    public static final String LAST_SEEN_POPUP_ID = "last_seen_popup_id";

}
