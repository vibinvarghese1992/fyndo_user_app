package user.fyndo.geospot.fyndouserapp;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by vibinvarghese on 08/09/16.
 */

public class PerformAsyncTask extends AsyncTask {

    private Context context;
    private String callingActivity, requestURL, method;
    private JSONObject postDataParams;

    public PerformAsyncTask(Context context, String callingActivity, String requestURL,
                            JSONObject postDataParams, String method) {
        super();
        this.context = context;
        this.callingActivity = callingActivity;
        this.requestURL = requestURL;
        this.postDataParams = postDataParams;
        this.method = method;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);

            if (method.equals("POST")) {
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(postDataParams.toString());

                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode = conn.getResponseCode();

            if (responseCode == 500) {
                Util.showSnackBar("Oops something seems wrong, we are on it. Please check back in sometime", ((MainActivity) context).recyclerView);
            }

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                callBackAfterNetworkResponse(response, true);
            } else {
                callBackAfterNetworkResponse(response, false);
            }

        } catch (Exception e) {
            if (e.getMessage().contains("resolve host")) {
                ((MainActivity) context).launchNoInternetDialog();
            } else {
                callBackAfterNetworkResponse(response, false);
            }
            e.printStackTrace();
        }

        return response;
    }

    private void callBackAfterNetworkResponse(final String response, final boolean isSuccess) {
        switch (callingActivity) {
            case "GetBizAreas":
                ((MainActivity) context).populateShopAreaSpinner(response, isSuccess);
                break;
            case "GetBizCategories":
                ((MainActivity) context).saveBizCategories(response, isSuccess);
                break;
            case "GetFeeds":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateFeeds(response, isSuccess);
                    }
                });
                break;
            case "UpVote":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).upVoteStatus(response, isSuccess);
                    }
                });
                break;
            case "GetCustomFeeds":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateCustomFeeds(response, isSuccess);
                    }
                });
                break;
            case "GetBizInfo":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateBizInfo(response, isSuccess);
                    }
                });
                break;
            case "GetBizFeeds":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateBizFeeds(response, isSuccess);
                    }
                });
                break;
            case "CheckIfUserExists":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).postUserExistsCheck(response, isSuccess);
                    }
                });
                break;
            case "CreateNewUser":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).postUserNewUserCreated(response, isSuccess);
                    }
                });
                break;
            case "DeepLinkFeed":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).launchDeepLinkView(response, isSuccess);
                    }
                });
                break;
            case "MapDirection":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).drawMapDirection(response, isSuccess);
                    }
                });
                break;
            case "ShareFeed":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).shareFeed(response, isSuccess);
                    }
                });
                break;
            case "SubCategoryFeeds":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateSubCategoryFeeds(response, isSuccess);
                    }
                });
                break;
            case "PopulateSubCategoryShops":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateSubCategoryStores(response, isSuccess);
                    }
                });
                break;
            case "InsertFavourite":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showSnackBar("Added to Favourites", ((MainActivity) context).recyclerView);
                    }
                });
                break;
            case "FavouriteFeeds":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateFavouriteFeeds(response, isSuccess);
                    }
                });
                break;
            case "FeedsOfFavouriteShop":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).populateFavouriteShopFeeds(response, isSuccess);
                    }
                });
                break;
            case "DeleteFavourite":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (((MainActivity) context).favouriteFeedsFragment != null)
                            ((MainActivity) context).favouriteFeedsFragment.makeRequest();
                    }
                });
                break;
            case "FavouriteStores":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (((MainActivity) context).favouriteShopsFragment != null)
                            try {
                                ((MainActivity) context).favouriteShopsFragment.populateFeedsArray(response, isSuccess);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }
                });
                break;
            case "Popup":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).launchServerPopup(response, isSuccess);
                    }
                });
                break;
            case "FollowBiz":
                ((MainActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
                break;

        }
    }
}
