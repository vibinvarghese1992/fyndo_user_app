package user.fyndo.geospot.fyndouserapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vibinvarghese on 27/09/16.
 */

public class StoreFragment extends Fragment {

    ArrayList<String> shopCategoryEatery;
    ListView listView;
    CategoryListAdapter categoryListAdapter;

    @SuppressLint("ValidFragment")
    public StoreFragment(ArrayList<String> shopCategoryEatery) {
        this.shopCategoryEatery = shopCategoryEatery;
    }

    public StoreFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.store_fragment, container, false);

        listView = (ListView) rootView.findViewById(R.id.store_list);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Gson gson = new Gson();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String json = prefs.getString(Constants.SELECTED_STORE_CATEGORIES, null);
        String abc = prefs.getString(Constants.SELECTED_STORE_CATEGORIES, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        ArrayList<String> selectedItems = gson.fromJson(json, type);

        categoryListAdapter = new CategoryListAdapter(getActivity(), shopCategoryEatery, selectedItems);
        listView.setAdapter(categoryListAdapter);
    }
}
