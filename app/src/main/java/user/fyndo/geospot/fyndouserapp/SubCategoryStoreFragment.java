package user.fyndo.geospot.fyndouserapp;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vibinvarghese on 23/11/16.
 */

public class SubCategoryStoreFragment extends Fragment {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView noFavText;

    ArrayList<StoreItemModel> storeArrayList = new ArrayList<>();
    StoreListRecyclerAdapter storeListRecyclerAdapter;

    SharedPreferences prefs;

    public SubCategoryStoreFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.subcat_store_fragment, container, false);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getContext().getApplicationContext();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("SubCategoryShopsFragment");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        storeListRecyclerAdapter = new StoreListRecyclerAdapter(getActivity(), storeArrayList);
        storeListRecyclerAdapter.notifyDataSetChanged();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(storeListRecyclerAdapter);
        noFavText = (TextView) rootView.findViewById(R.id.no_fav_text);


        progressBar.setVisibility(View.GONE);
        makeRequest();

        return rootView;
    }

    protected void makeRequest() {
        JSONObject postDataParams = new JSONObject();

        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        storeArrayList.clear();
        storeListRecyclerAdapter.notifyDataSetChanged();
        noFavText.setVisibility(View.GONE);

        String json;
        Gson gson = new Gson();
        JSONArray level2Cat;
        JSONObject subCategoryObject;
        JSONArray filterCategories = new JSONArray();

        json = gson.toJson(((MainActivity) getActivity()).selectedMen);
        if (json == null)
            return;

        try {

            level2Cat = new JSONArray(json);
            subCategoryObject = new JSONObject();
            if (level2Cat.length() > 0) {
                subCategoryObject.put("userCatLevel1", "Men");
                subCategoryObject.put("userCatLevel2", level2Cat);
                filterCategories.put(subCategoryObject);
            }

            json = gson.toJson(((MainActivity) getActivity()).selectedWomen);
            level2Cat = new JSONArray(json);
            subCategoryObject = new JSONObject();
            if (level2Cat.length() > 0) {
                subCategoryObject.put("userCatLevel1", "Women");
                subCategoryObject.put("userCatLevel2", level2Cat);
                filterCategories.put(subCategoryObject);
            }

            postDataParams.put("filterCategories", filterCategories);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        PerformAsyncTask performAsyncTask = new PerformAsyncTask(getActivity(), "PopulateSubCategoryShops",
                Constants.SELECT_BIZ_INFOS_BY_CATEGORY, postDataParams, "POST");

        performAsyncTask.execute();
    }

    protected void populateBizFeeds(String response, boolean isSuccess) {
        if (!isSuccess) {
            Util.showSnackBar("Something Went wrong, please try again", recyclerView);
            return;
        }

        try {
            populateFeedsArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void populateFeedsArray(String response) throws JSONException {
        final JSONArray feedsArray = new JSONArray(response);

        if (feedsArray.length() == 0) {
            noFavText.setVisibility(View.VISIBLE);
        }

        for (int i = 0; i < feedsArray.length(); i++) {

            JSONObject feed = feedsArray.getJSONObject(i);
            if (feed.isNull("bizProfile"))
                continue;

            StoreItemModel storeItemModel = new StoreItemModel();
            storeItemModel.setShopName(feed.getString("bizName"));

            String shopAreaTextServer = feed.getString("bizStreetAddress").replace("\n", " ");
            String extractedArea = shopAreaTextServer;
            String[] areaArray = shopAreaTextServer.split(" ");
            for (int k = 0; k < areaArray.length; k++) {
                if (areaArray[k].equals("Chennai") || areaArray[k].equals("Chennai,")) {
                    extractedArea = areaArray[k - 1];
                    break;
                }
            }
            storeItemModel.setShopArea(extractedArea);

            storeItemModel.setBizId(feed.getInt("bizId"));
            storeItemModel.setBizCategories(feed.getString("bizCategory"));
            storeItemModel.setShopImage(feed.getString("bizProfile"));
            storeItemModel.setShopPhone(feed.getString("bizMobile"));
            storeItemModel.setLat(feed.getDouble("gpsLatitude"));
            storeItemModel.setLng(feed.getDouble("gpsLongitude"));

            storeArrayList.add(storeItemModel);
        }

        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                progressBar.setVisibility(View.GONE);
                                                recyclerView.setVisibility(View.VISIBLE);
                                                storeListRecyclerAdapter.notifyDataSetChanged();
                                                //feedListRecyclerAdapter.notifyItemRangeChanged(0, feedArrayList.size());

                                            }
                                        }
            );

    }
}
